<?php
require_once ("config.inc.php");

if (!frOSt_isLoggedIn())
{
  exit ("Please log into frOSt in order to access this page.");
}

$database = database();

$suggestionStatuses = array("completed", "pending", "inProgress", "declined");
if (isset($_POST["process"]) && $_POST["process"] == "suggestions")
{
	$suggestionId = $_POST["id"];
	if ($database->querySingle("SELECT count(*) FROM suggestions WHERE suggestionID='" . $database->escapeString($suggestionId) . "'") > 0)
	{
	  if (in_array($_POST["status"], $suggestionStatuses))
	  {
	    $database->exec("UPDATE suggestions SET status='" . $database->escapeString($_POST["status"]) . "' WHERE suggestionID='" . $database->escapeString($suggestionId) .
			"'");
	  }
	  else
	  {
	    echo "no2";
	  }
	}
	else
	{
	  echo "no1";
	}
}
?>
<table>
  <tr>
    <td>Suggestion</td>
	<td>Votes</td>
	<td>Status</td>
  </tr>
  <?php
    $suggestions = $database->query("SELECT suggestionID, name, votes, status FROM suggestions ORDER BY name ASC");
	while ($suggestion = $suggestions->fetchArray())
	{
	  echo "<tr>";
	  echo "<td>", $suggestion["name"], "</td>";
	  echo "<td>", $suggestion["votes"], "</td>";
	  echo "<td><form method=\"post\" action=\".\"><input type=\"hidden\" name=\"process\" value=\"suggestions\" /><input type=\"hidden\" name=\"id\" value=\"",
		$suggestion["suggestionID"], "\" /><select name=\"status\">";
	  foreach ($suggestionStatuses as $status)
	  {
	    echo "<option value=\"", $status, "\"", ($status == $suggestion["status"] ? " selected=\"selected\"" : ""), ">", $status, "</option>";
	  }
	  echo "</select> <input type=\"submit\" value=\"Save\" /></form></td>";
	  echo "</tr>";
	}
  ?>
</table>