<?php
require_once ("../lib/config.inc.php");
$database = database();

function place($question, $column, $topPosition, &$questions, &$currentIndex, &$placementID, &$leftHeight, &$centerHeight, &$rightHeight,
    &$funRatio, &$postsSinceFun, &$funCount)
{
    global $database;
    
    if (($question["width"] == "one" && $column == "center"))// || $question["width"] == "two" || $question["width"] == "three")
    {
        $sortHeight = $topPosition + $question["clientHeight"];
    }
    else
    {
        $sortHeight = $topPosition;
    }
    
    if (!$database->exec("INSERT INTO aboutBoxes(`column`,`sortHeight`,`boxType`,`answer`) VALUES('" . $database->escapeString($column) . "','" .
        $database->escapeString($sortHeight) . "','answer','" . $database->escapeString($question["aboutID"]) . "')"))
    {
        exit("Error adding to database.");
    }
    
    if ($question["width"] == "one")
    {
        if ($column == "left")
        {
            $leftHeight = $topPosition + $question["clientHeight"] + 4;
        }
        else if ($column == "center")
        {
            $centerHeight = $topPosition + $question["clientHeight"] + 4;
        }
        else
        {
            $rightHeight = $topPosition + $question["clientHeight"] + 4;
        }
    }
    else if ($question["width"] == "two")
    {
        if ($column == "left")
        {
            $leftHeight = $topPosition + $question["clientHeight"] + 4;
            $centerHeight = $topPosition + $question["clientHeight"] + 4;
        }
        else
        {
            $centerHeight = $topPosition + $question["clientHeight"] + 4;
            $rightHeight = $topPosition + $question["clientHeight"] + 4;
        }
    }
    else
    {
        $leftHeight = $topPosition + $question["clientHeight"] + 4;
        $centerHeight = $topPosition + $question["clientHeight"] + 4;
        $rightHeight = $topPosition + $question["clientHeight"] + 4;
    }
    echo "-- " . $leftHeight . ", " . $centerHeight . ", " . $rightHeight . " (" . $column . ", " . $question["width"] . ", " .
        $question["clientHeight"] . ")\n";
    array_splice($questions, $currentIndex, 1);
    $currentIndex = 0;
    $placementID++;
    if ($question["isFun"])
    {
        $postsSinceFun = 0;
        $funCount--;
    }
    else
    {
        $postsSinceFun++;
    }
    
    if ($funCount > 0)
    {
        $funRatio = round(count($questions) / $funCount);
    }
    else
    {
        $funRatio = 0;
    }
}

$database->exec("DELETE FROM aboutBoxes WHERE locked='0'");
$database->exec("ALTER TABLE aboutBoxes AUTO_INCREMENT = 1");

$questions = array();
$questionResults = $database->query("SELECT aboutID, width, isFun, clientHeight FROM about LEFT JOIN aboutBoxes ON " .
    "about.aboutID = aboutBoxes.answer WHERE locked = NULL OR locked = 0 ORDER BY answeredOn ASC");
$funCount = 0;
while ($question = $questionResults->fetchArray())
{
    $questions[] = array("aboutID" => $question["aboutID"], "width" => $question["width"],
        "isFun" => ($question["isFun"] == "1"), "clientHeight" => $question["clientHeight"], "lastComparedFor" => null,
        "lastComparedForFun" => null);
    if ($question["isFun"] == "1")
    {
        $funCount++;
    }
}

define("THRESHOLD", 10);
$leftHeight = 0;
$centerHeight = 0;
$rightHeight = 0;
$placementID = 0;
$currentIndex = 0;
$funRatio = ($funCount > 0 ? round(count($questions) / $funCount) : 0);
$postsSinceFun = 0;
while (count($questions) > 0)
{
    $question = $questions[$currentIndex];
    
    if ($postsSinceFun >= $funRatio && !$question["isFun"])
    {
        if ($question["lastComparedForFun"] !== $placementID)
        {
            $question["lastComparedForFun"] = $placementID;
            $currentIndex++;
            if ($currentIndex >= count($questions))
            {
                $currentIndex = 0;
            }
            continue;
        }
    }
    
    if ($question["width"] == "three")
    {
        if ($question["lastComparedFor"] === $placementID || (abs($leftHeight - $centerHeight) <= THRESHOLD &&
            abs($leftHeight - $rightHeight) <= THRESHOLD && abs($centerHeight - $rightHeight) <= THRESHOLD))
        {
            place($question, "left", max($leftHeight, $centerHeight, $rightHeight), $questions, $currentIndex, $placementID, $leftHeight,
                $centerHeight, $rightHeight, $funRatio, $postsSinceFun, $funCount);
            continue;
        }
    }
    else if ($question["width"] == "two")
    {
        $tempLeft = $leftHeight + $question["clientHeight"];
        $tempCenter = $centerHeight + $question["clientHeight"];
        $tempRight = $rightHeight + $question["clientHeight"];
        
        $tempLeftTwo = max($tempLeft, $tempCenter);
        $tempCenterTwo = max($tempCenter, $tempRight);
        
        $leftValid = (abs($tempLeft - $tempCenter) <= THRESHOLD);
        $centerValid = (abs($tempCenter - $tempRight) <= THRESHOLD);
        
        if (($question["lastComparedFor"] === $placementID && $tempLeftTwo <= $tempCenterTwo) || ($leftValid && (!$centerValid ||
            $tempLeftTwo <= $tempCenterTwo)))
        {
            place($question, "left", max($leftHeight, $centerHeight), $questions, $currentIndex, $placementID, $leftHeight, $centerHeight,
                $rightHeight, $funRatio, $postsSinceFun, $funCount);
            continue;        
        }
        else if ($question["lastComparedFor"] === $placementID || $centerValid)
        {
            place($question, "center", max($centerHeight, $rightHeight), $questions, $currentIndex, $placementID, $leftHeight, $centerHeight,
                $rightHeight, $funRatio, $postsSinceFun, $funCount);
            continue;
        }
    }
    else if ($question["width"] == "one")
    {
        $tempLeft = $leftHeight + $question["clientHeight"];
        $tempCenter = $centerHeight + $question["clientHeight"];
        $tempRight = $rightHeight + $question["clientHeight"];
        if ($tempLeft <= $tempCenter && $tempLeft <= $tempRight)
        {
            place($question, "left", $leftHeight, $questions, $currentIndex, $placementID, $leftHeight, $centerHeight, $rightHeight,
                $funRatio, $postsSinceFun, $funCount);
            continue;
        }
        else if ($tempCenter <= $tempRight)
        {
            place($question, "center", $centerHeight, $questions, $currentIndex, $placementID, $leftHeight, $centerHeight, $rightHeight,
                $funRatio, $postsSinceFun, $funCount);
            continue;
        }
        else
        {
            place($question, "right", $rightHeight, $questions, $currentIndex, $placementID, $leftHeight, $centerHeight, $rightHeight,
                $funRatio, $postsSinceFun, $funCount);
            continue;
        }
    }
    else
    {
        exit("Invalid size");
    }
    $questions[$currentIndex]["lastComparedFor"] = $placementID;
    $currentIndex++;
    if ($currentIndex >= count($questions))
    {
        $currentIndex = 0;
    }
}

exit ("Done\n");
?>