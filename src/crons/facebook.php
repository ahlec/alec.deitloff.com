<?php
require_once ("../lib/config.inc.php");
require_once (DOCUMENT_ROOT . "/crons/facebook/facebook.php");
$database = database();

$config = array(
    'appId' => '398544996903698',
    'secret' => '957d9b366a5acf845d050e5fde3b7124',
  );
$facebook = new Facebook($config);
$facebook->setAccessToken("CAAFqeXz3qxIBAOdaZBoPc2ndPQixVu3NPgvvpeqANh1ZBmTzALmVRF3Ico7L9fqkfDrukDwdYH65LpQu1WACYVHedrKyTmljkxBCDTTYkgaFijuUHSLWO9lSDEesn1viIJDo1qS7XgZANynT9TwIctKkBSkwg3uIAiEc4L25LyJxhVscB3XV21dL0LqVisZD");

/*

Step 1:
http://www.facebook.com/dialog/oauth?scope=read_stream&client_id=398544996903698&redirect_uri=http://jacob.deitloff.com/

Step 2:
https://graph.facebook.com/oauth/access_token?client_id=398544996903698&redirect_uri=http://jacob.deitloff.com/&client_secret=957d9b366a5acf845d050e5fde3b7124&code=

*/
$times = 0;
$done = false;
while (true)
{
    if ($times == 10)
    {
        if (mail("5132629675@vtext.com", "", "Sorry to bother you, Alec. The Facebook OAuth key has expired, " .
            "and the Facebook crawler can't work. Can you update it? Thanks; you're the best :)",
            "From: Alec Deitloff <alec@deitloff.com>\r\n", "-falec@deitloff.com"))
        {
            exit("Invalid OAuth key. Text alert sent.\n");
        }
        else
        {
            exit("Invalid OAuth key. Text alert could not be sent.\n");
        }
    }
    try
    {
        $statuses = $facebook->api("/me/statuses", "GET");
        $done = true;
    }
    catch (Exception $e)
    {
        $done = false;
    }
    
    if ($done)
    {
        break;
    }
    
    $times++;
}

$avatar = file_get_contents("http://graph.facebook.com/alec.deitloff/picture?width=140&height=140");
$avatarHash = md5($avatar);
if ($database->querySingle("SELECT count(*) FROM socialMediaAvatars WHERE site='facebook' AND hash='" .
    $database->escapeString($avatarHash) . "'") > 0)
{
    $avatarID = $database->querySingle("SELECT avatarID FROM socialMediaAvatars WHERE site='facebook' AND hash='" .
        $database->escapeString($avatarHash) . "' LIMIT 1");
}
else
{
    do
    {
        $localFile = "facebook" . rand() . ".jpg";
    } while (file_exists(DOCUMENT_ROOT . "/images/avatars/" . $localFile));
    if (file_put_contents(DOCUMENT_ROOT . "/images/avatars/" . $localFile, $avatar) === false)
    {
        exit ("Error with moving new avatar to the avatar directory.");
    }
    
    if ($database->exec("INSERT INTO socialMediaAvatars(`site`,`localFile`,`hash`) VALUES('facebook','" .
        $database->escapeString($localFile) . "','" . $database->escapeString($avatarHash) . "')"))
    {
        $avatarID = $database->getLastAutoInc();
    }
    else
    {
        unlink(DOCUMENT_ROOT . "/images/avatars/" . $localFile);
        exit ("Error with establishing a new avatar.");
    }
}

foreach ($statuses["data"] as $status)
{
    if (!isset($status["message"]))
    {
        continue;
    }
    
    $post = array("avatar" => $avatarID,
        "dateTime" => date("Y-m-d H:i:s", strtotime($status["updated_time"])),
        "fullText" => $status["message"],
        "remoteID" => $status["id"]);
        
    if ($database->querySingle("SELECT count(*) FROM socialMediaPosts WHERE `site`='facebook' AND remoteID='" .
        $database->escapeString($post["remoteID"]) . "'") > 0)
    {
        continue;
    }
    
    if ($database->exec("INSERT INTO socialMediaPosts(`site`,`avatar`,`dateTime`,`fullText`,`siteURL`,`remoteID`) VALUES('facebook','" .
        $database->escapeString($post["avatar"]) . "','" . $database->escapeString($post["dateTime"]) . "','" .
        $database->escapeString($post["fullText"]) . "','http://www.facebook.com/alec.deitloff/posts/" .
        $database->escapeString($post["remoteID"]) . "','" . $database->escapeString($post["remoteID"]) . "')"))
    {
        $postID = $database->getLastAutoInc();
        if (!$database->exec("INSERT INTO console(`directory`,`command`,`dateTime`,`isSocialMedia`,`socialMediaPostID`) VALUES('/remote/facebook/', '" .
            $database->escapeString("POST_STATUS \"" . escapeConsoleString($post["fullText"]) . "\"") . "','" . $database->escapeString($post["dateTime"]) .
            "','1','" . $database->escapeString($postID) . "')"))
        {
            $database->exec("DELETE FROM socialMediaPosts WHERE postID='" . $database->escapeString($postID) . "'");
        }
    }
    
    var_dump($post);
}
?>