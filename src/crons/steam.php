<?php
require_once ("../lib/config.inc.php");
require_once (DOCUMENT_ROOT . "/crons/steam/steam-condenser.php");

$database = database();
$profile = $database->querySingle("SELECT accountName, profileURL FROM socialMedia WHERE siteHandle='steam' LIMIT 1", true);
$dbProfile = $profile;

$times = 0;
$done = false;
while (true)
{
    if ($times == 10)
    {
        exit("Encountered too many failed attempts.");
    }
    try
    {
        $profile = SteamId::create($profile["accountName"]);
        $done = true;
    }
    catch (Exception $e)
    {
        $done = false;
    }
    if ($done)
    {
        break;
    }
    $times++;
}

$database = database();
$avatar = file_get_contents($profile->getFullAvatarUrl());
$avatarHash = md5($avatar);
if ($database->querySingle("SELECT count(*) FROM socialMediaAvatars WHERE site='steam' AND hash='" .
    $database->escapeString($avatarHash) . "'") > 0)
{
    $avatarID = $database->querySingle("SELECT avatarID FROM socialMediaAvatars WHERE site='steam' AND hash='" .
        $database->escapeString($avatarHash) . "' LIMIT 1");
}
else
{
    do
    {
        $localFile = "steam" . rand() . ".jpg";
    } while (file_exists(DOCUMENT_ROOT . "/images/avatars/" . $localFile));
    if (file_put_contents(DOCUMENT_ROOT . "/images/avatars/" . $localFile, $avatar) === false)
    {
        exit ("Error with moving new avatar to the avatar directory.");
    }
    
    if ($database->exec("INSERT INTO socialMediaAvatars(`site`,`localFile`,`hash`) VALUES('steam','" .
        $database->escapeString($localFile) . "','" . $database->escapeString($avatarHash) . "')"))
    {
        $avatarID = $database->getLastAutoInc();
    }
    else
    {
        unlink(DOCUMENT_ROOT . "/images/avatars/" . $localFile);
        exit ("Error with establishing a new avatar.");
    }
}

$times = 0;
$done = false;
while ($times < 10)
{
    try
    {
        $games = $profile->getGames();
        $done = true;
    }
    catch (Exception $e)
    {
        $done = false;
    }
    if ($done)
    {
        break;
    }
    $times++;
}
if (!$done)
{
    exit("Encountered too many problems getting games for jyavoc\n");
}

function compareAchievements($achievementA, $achievementB)
{
    if ($achievementA["time"] == $achievementB["time"])
    {
        return 0;
    }
    return ($achievementA["time"] < $achievementB["time"]) ? -1 : 1;
}

$isFirstGame = true;
foreach ($games as $game)
{
    if (!$game->hasStats())
    {
        continue;
    }
	
	if (!$isFirstGame)
	{
		echo "</fieldset>";
	}
	else
	{
		$isFirstGame = false;
	}
	
    $times = 0;
    $done = false;
    while ($times < 10)
    {
        try
        {
            $stats = $profile->getGameStats($game->getAppId());
            $done = true;
        }
        catch (Exception $e)
        {
            $done = false;
        }
        if ($done)
        {
            break;
        }
        $times++;
    }
	
	echo "<fieldset><legend>", $game->getName(), "</legend>";
    if (!$done)
    {
        echo "Encountered too many problems getting stats for \"" . $game->getName() . "\"<br />\n";
        continue;
    }
	
	if ($stats->getHoursPlayed() == 0)
	{
		echo "No hours played for game \"", $game->getName(), "\"<br />\n";
		continue;
	}
	
	echo "<h2>", $game->getName(), "</h2>\n";
    if ($database->querySingle("SELECT count(*) FROM steamGames WHERE appID='" . $database->escapeString($game->getAppId()) . "'") == 0)
    {
        $gameIconURL = $game->getLogoUrl();
        if ($gameIconURL != null)
        {
            $gameIconData = file_get_contents($gameIconURL);
            if (file_put_contents(DOCUMENT_ROOT . "/images/steamGames/" . $game->getAppId() . ".jpg", $gameIconData) === false)
            {
                continue;
            }
        }
        if (!$database->exec("INSERT INTO steamGames(`appID`,`name`,`hasLogo`) VALUES('" . $database->escapeString($game->getAppId()) . "','" .
            $database->escapeString($game->getName()) . "','" . ($gameIconURL != null ? "1" : "0") . "')"))
        {
            continue;
        }
    }
   
    $achievements = $stats->getAchievements();
    $newAchievementsUnlocked = array();
    foreach ($achievements as $achievement)
    {
        if (!$achievement->isUnlocked())
        {
            continue;
        }
		
		echo "Touching achievement: '", $achievement->getName(), "<br />";
        
        if ($database->querySingle("SELECT count(*) FROM steamAchievements WHERE apiName='" . $database->escapeString($achievement->getApiName()) .
            "' AND appID='" . $database->escapeString($game->getAppId()) . "'") > 0)
        {
            continue;
        }
        
        if ($database->exec("INSERT INTO steamAchievements(`apiName`,`name`,`description`,`appID`,`achievedOn`,`avatar`) VALUES('" .
            $database->escapeString($achievement->getApiName()) . "','" . $database->escapeString($achievement->getName()) . "','" .
            $database->escapeString($achievement->getDescription()) . "','" .
            $database->escapeString($game->getAppId()) . "','" . $database->escapeString(date("Y-m-d H:i:s", $achievement->getTimestamp())) .
            "','" . $database->escapeString($avatarID) . "')"))
        {
            $achievementID = $database->getLastAutoInc();
            $achievementIcon = file_get_contents($achievement->getIconClosedUrl());
            if (file_put_contents(DOCUMENT_ROOT . "/images/steamAchievements/" . $achievementID . ".jpg", $achievementIcon) == false)
            {
                $database->exec("DELETE FROM steamAchievements WHERE achievementID='" . $database->escapeString($achievementID) . "'");
                continue;
            }
            $newAchievementsUnlocked[] = array("name" => $achievement->getName(),
                "time" => $achievement->getTimestamp(), "achievementID" => $achievementID,
                "description" => $achievement->getDescription());
        }
    }
    
    if (count($newAchievementsUnlocked)> 0)
    {
        usort($newAchievementsUnlocked, "compareAchievements");
        
        $unlockGroups = array();
        $lastUnlock = 0;
        foreach ($newAchievementsUnlocked as $achievement)
        {
            if ($achievement["time"] - $lastUnlock > 3600)
            {
                $unlockGroups[] = array();
                $lastUnlock = $achievement["time"];
            }
            $unlockGroups[count($unlockGroups) - 1][] = $achievement;
        }
        
        foreach ($unlockGroups as $group)
        {
            if (count($group) == 0)
            {
                continue;
            }
            
            $achievementList = null;
            $lastUnlocked = 0;
            $socialMediaPost = "";
            foreach ($group as $achievement)
            {
                if ($achievementList == null)
                {
                    $achievementList = "";
                }
                else
                {
                    $achievementList .= " ";
                }
                $achievementList .= "\"" . escapeConsoleString($achievement["name"]) . "\"";
                $socialMediaPost .= "<tr><td><img src=\"./images/steamAchievements/" .
                    $achievement["achievementID"] . ".jpg\"></td><td><strong>" . $achievement["name"] .
                    "</strong>" . $achievement["description"] . "</td></tr>";
                if ($achievement["time"] > $lastUnlocked)
                {
                    $lastUnlocked = $achievement["time"];
                }
            }
            
            $socialMediaPost = "<a href=\"" . $dbProfile["profileURL"] . "/stats/" .
                $game->getAppId() . "\" target=\"_blank\">" .
                $game->getName() . "</a> achievement" . (count($group) != 1 ? "s" : "") .
                " unlocked: <table class=\"steamAchievements\">" . $socialMediaPost . "</table>";
            if ($database->exec("INSERT INTO socialMediaPosts(`site`,`avatar`,`dateTime`,`fullText`,`siteURL`,`isHTML`) VALUES('steam','" .
                $database->escapeString($avatarID) . "','" . $database->escapeString(date("Y-m-d H:i:s",
                $lastUnlocked)) . "','" . $database->escapeString($socialMediaPost) . "','/stats/" .
                $database->escapeString($game->getAppId()) . "','1')"))
            {
                $postID = $database->getLastAutoInc();
                $database->exec("INSERT INTO console(`directory`,`command`,`dateTime`,`isSocialMedia`,`socialMediaPostID`) VALUES('/remote/steam/" .
                    $database->escapeString($database->escapeString(str_replace(" ", "_", preg_replace("/[^0-9a-zA-z ]/", "", $game->getName())))) .
                    "/','ACHIEVE " . $database->escapeString($achievementList) . "','" .
                    $database->escapeString(date("Y-m-d H:i:s", $lastUnlocked)) .
                    "','1','" . $database->escapeString($postID) . "')");
            }
        }
    }
}

echo "<h2>[SCRIPT COMPLETE]</h2>";
?>