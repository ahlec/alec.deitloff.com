<?php
define("GOOGLE_API_KEY", "AIzaSyCfsnLJhyMzZOqmomSkiMtPSl3e06zSuHg");
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
require_once (__DIR__ . "/../lib/config.inc.php");
$database = database();

function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}
function beginsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0)
    {
        return true;
    }
    return (substr($haystack, $length) === $needle);
}

$queued = $database->query("SELECT queueID, input, submitted FROM suggestionsQueue WHERE triesToParse < 1");
while ($suggestion = $queued->fetchArray())
{
	$search_results_raw = file_get_contents("https://www.googleapis.com/customsearch/v1?key=" .
		GOOGLE_API_KEY . "&cx=013036536707430787589:_pqjad5hr1a&q=" .
		urlencode($suggestion["input"]) . "%20wikipedia&alt=json");
	$search_results = json_decode($search_results_raw);
	if ($search_results === null || !isset($search_results->items))
	{
		var_dump($search_results_raw);
		exit();
	}
	
	/*if (!in_array("items", $search_results))
	{
	echo "<pre>";
		var_dump($search_results);
		echo "skipping";
		continue;
	}*/
	
	$wikipedia_link = null;
	foreach ($search_results->items as $page)
	{
		if (strpos($page->link, "http://en.wikipedia.org/wiki/") === 0)
		{
			$wikipedia_link = urldecode($page->link);
			break;
		}
	}
	
	if ($wikipedia_link === null)
	{
		$database->exec("UPDATE suggestionsQueue SET triesToParse = triesToParse + 1 WHERE queueID='" .
			$suggestion["queueID"] . "'");
		echo "none found";
		continue;
	}
	
	$wikipedia_title = mb_substr($wikipedia_link, mb_strlen("http://en.wikipedia.org/wiki/"));
	
	$wikipedia_query_raw = file_get_contents("http://en.wikipedia.org/w/api.php?action=query" .
		"&titles=" . $wikipedia_title . "&prop=categories&cllimit=100&format=json");
	$wikipedia_results = json_decode($wikipedia_query_raw);
	if ($wikipedia_results === null)
	{
		$database->exec("UPDATE suggestionsQueue SET triesToParse = triesToParse + 1 WHERE queueID='" .
			$suggestion["queueID"] . "'");
		echo "Error gettting information from wikipedia";
		continue;
	}
	
	$name = preg_replace("/_/", " ", $wikipedia_title);
	if ($database->querySingle("SELECT count(*) FROM suggestions WHERE name='" . $database->escapeString($name) . "'") > 0)
	{
		$existing_suggestion_id = $database->querySingle("SELECT suggestionID FROM suggestions WHERE name='" .
            $database->escapeString($name) . "' LIMIT 1");
		$database->exec("UPDATE suggestions SET votes = votes + 1 WHERE suggestionID='" . $existing_suggestion_id .
			"' AND status='pending'");
		$database->exec("DELETE FROM suggestionsQueue WHERE queueID='" . $suggestion["queueID"] . "'");
		continue;
	}
	
	//echo "<pre>";
	$pages_object_vars = get_object_vars($wikipedia_results->query->pages);
	$wikipedia_page = array_shift($pages_object_vars);
	//var_dump($wikipedia_page);
	//echo "</pre>";
	
	$entry_type = null;
	$parsed_categories = array();
	foreach ($wikipedia_page->categories as $category)
	{
		$category_name = mb_substr($category->title, mb_strlen("Category:"));
		$parsed_categories[] = $category_name;
		$category_set_now = true;
		$quit_categorization = false;
		switch ($category_name)
		{
			case "Nintendo 3DS games":
			{
				$entry_type = "3ds game";
				break;
			}
			case "Nintendo DS games":
			case "Nintendo DS-only games":
			{
				$entry_type = "ds game";
				break;
			}
			case "Game Boy Advance games":
			{
				$entry_type = "gba game";
				break;
			}
			case "Game Boy Color games":
			{
				$entry_type = "gbc game";
				break;
			}
			case "Nintendo GameCube games":
			{
				$entry_type = "gamecube game";
				break;
			}
			case "Nintendo Wii games":
			case "WiiWare games":
			case "Wii games":
			{
				$entry_type = "wii game";
				break;
			}
			case "PlayStation games":
			{
				$entry_type = "ps1 game";
				break;
			}
			case "PlayStation 2 games":
			{
				$entry_type = "ps2 game";
				break;
			}
			case "PlayStation 3 games":
			{
				$entry_type = "ps3 game";
				break;
			}
			case "PlayStation Portable games":
			{
				$entry_type = "psp game";
				break;
			}
			case "Xbox games":
			{
				$entry_type = "xbox game";
				break;
			}
			case "Xbox 360 games":
			{
				$entry_type = "xbox360 game";
				break;
			}
			case "Anime series":
			{
				$entry_type = "anime";
				$quit_categorization = true;
				break;
			}
			case "Manga series":
			case "Tokyopop titles":
			{
				$entry_type = "manga";
				break;
			}
			case "Webcomics":
			{
				$entry_type = "webcomic";
				break;
			}
			case "DC Comics titles":
			case "Marvel Comics titles":
			case "Dark Horse Comics titles":
			case "Helix titles":
			case "Comics publications":
			case "Widescreen comics":
			case "Superhero comics":
			case "Fantasy comics‎":
			case "Horror comics‎":
			case "Historical comics‎":
			case "Humor comics":
			case "Mystery comics":
			case "Action-adventure comics‎":
			case "Crossover comics":
			case "Techno-thriller comics‎":
			case "Spy comics‎":
			case "Science fiction comics‎":
			case "War comics‎":
			{
				$entry_type = "comic";
				break;
			}
			case "Fantasy MMORPGs":
			case "Community-style MMORPGs":
			case "Historical MMORPGs‎":
			case "MMORPGs in space‎":
			case "Nautical MMORPGs‎":
			case "Science-fiction MMORPGs‎":
			case "Sports management MMORPGs":
			case "Text-based MMORPGs‎":
			case "Virtual reality communities":
			{
				$entry_type = "mmorpg";
				$quit_categorization = true;
				break;
			}
			case "Linux games‎":
			case "Mac OS games":
			case "DOS games‎":
			case "PC games":
			case "Windows games":
			{
				$entry_type = "computer game";
				break;
			}
            case "American web series":
			case "Animated internet series‎":
			case "Canadian web series‎":
			case "Comedy web series‎":
			case "Drama web series‎":
            case "LGBT-related web series":
			case "Non-fiction web series‎":
			case "Science fiction web series‎":
			case "Virtual series‎":
			case "YouTube series‎":
			case "Web series":
			{
				$entry_type = "web series";
				$quit_categorization = true;
				break;
			}
			case "Blooper shows":
			case "Entertainment news shows‎":
			case "Gambling television programs‎":
			case "Infotainment‎":
			case "Television magic shows‎":
			case "Motorcycle TV shows":
			case "Neo-noir":
			case "Paranormal television‎":
			case "Television programs featuring puppetry":
			case "Television sketch shows‎":
			case "Superhero television programs‎":
			case "Television series about journalism‎":
			case "Television series with live action and animation‎":
			case "World War II television programmes‎":
			{
				$entry_type = "television show";
				break;
			}
			default:
			{
				$category_set_now = false;
				break;
			}
		}
		
		if ($quit_categorization)
		{
			break;
		}
		
		if (!$category_set_now)
		{
			if (endsWith($category_name, " television series"))
			{
				$entry_type = "television show";
			}
			else if (endsWith($category_name, " films"))
			{
				$entry_type = "movie";
			}
			else if (endsWith($category_name, " webcomics"))
			{
				$entry_type = "webcomic";
			}
			else if (endsWith($category_name, " novels") || endsWith($category_name, " books") || beginsWith($category_name, "Books "))
			{
				$entry_type = "novel";
			}
            else if (endsWith($category_name, " mythology"))
            {
                $entry_type = "mythology";
            }
		}
	}
	
	if ($entry_type === null)
	{
		echo "COULD NOT CATEGORIZE '" . $suggestion["input"] . "', given the following categories:<br />\n";
		echo "<pre>";
		var_dump($parsed_categories);
		echo "</pre>";
		$database->exec("UPDATE suggestionsQueue SET triesToParse = triesToParse + 1 WHERE queueID='" .
			$suggestion["queueID"] . "'");
		continue;
	}
	
	if ($database->exec("INSERT INTO suggestions(name, suggestionType, submitted, votes) VALUES('" .
        $database->escapeString($name) . "','" . $entry_type . "','" . $suggestion["submitted"] . "','1')"))
	{
		$database->exec("DELETE FROM suggestionsQueue WHERE queueID='" . $suggestion["queueID"] . "'");
	}
	
	//echo $suggestion["input"] . " ==> '" . $wikipedia_title . "'<br />\n";
}
?>
