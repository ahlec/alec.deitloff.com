<?php
require_once ("../lib/config.inc.php");
define("TUMBLR_API_KEY", "Sol1jha8nuMkIX0iHQ99ovpDuAfkJ7bMOITkM6tTDKMa0BDjmm");

$database = database();
$avatar = file_get_contents("http://api.tumblr.com/v2/blog/ahlec-si.tumblr.com/avatar/96");
$avatarHash = md5($avatar);
if ($database->querySingle("SELECT count(*) FROM socialMediaAvatars WHERE site='tumblr' AND hash='" .
    $database->escapeString($avatarHash) . "'") > 0)
{
    $avatarID = $database->querySingle("SELECT avatarID FROM socialMediaAvatars WHERE site='tumblr' AND hash='" .
        $database->escapeString($avatarHash) . "' LIMIT 1");
}
else
{
    do
    {
        $localFile = "tumblr" . rand() . ".jpg";
    } while (file_exists(DOCUMENT_ROOT . "/images/avatars/" . $localFile));
    if (file_put_contents(DOCUMENT_ROOT . "/images/avatars/" . $localFile, $avatar) === false)
    {
        exit ("Error with moving new avatar to the avatar directory.");
    }
    
    if ($database->exec("INSERT INTO socialMediaAvatars(`site`,`localFile`,`hash`) VALUES('tumblr','" .
        $database->escapeString($localFile) . "','" . $database->escapeString($avatarHash) . "')"))
    {
        $avatarID = $database->getLastAutoInc();
    }
    else
    {
        unlink(DOCUMENT_ROOT . "/images/avatars/" . $localFile);
        exit ("Error with establishing a new avatar.");
    }
}

$posts = file_get_contents("http://api.tumblr.com/v2/blog/ahlec-si.tumblr.com/posts?api_key=" .
	TUMBLR_API_KEY . "&reblog_info=true");
$posts = json_decode($posts);

function getTitleFromCaption($caption)
{
	
}

function prepareText($postId, $text, &$postFiles, $photoIndex = 0)
{
	// Process images
	$imageMatches = array();
	if (preg_match_all("/<img ([^>]*?)src=\"([^\"]*)\"/", $text, $imageMatches, PREG_SET_ORDER) > 0)
	{
		$uniqueImages = array();
		foreach ($imageMatches as $image)
		{
			if (in_array($image[2], $uniqueImages))
			{
				continue;
			}
			
			$imageFile = file_get_contents($image[2]);
			$fileInfo = pathinfo($image[2]);
			
			$localFile = $postId . "_" . $photoIndex . "." . $fileInfo["extension"];
			$fileTries = 0;
			while (file_exists(DOCUMENT_ROOT . "/images/tumblr/" . $localFile))
			{
				$fileTries++;
				$localFile = $postId . "_" . $photoIndex . "_" . $fileTries . "." . $fileInfo["extension"];
			}
			
			if (file_put_contents(DOCUMENT_ROOT . "/images/tumblr/" . $localFile, $imageFile) === false)
			{
				foreach ($postFiles as $postFile)
				{
					unlink($postFile);
				}
				exit ("Error with moving picture to the image/tumblr directory.");
			}
			
			$postFiles[] = DOCUMENT_ROOT . "/images/tumblr/" . $localFile;  
			$uniqueImages[] = $image[2];
			$text = str_replace("src=\"" . $image[2] . "\"",
								"src=\"/images/tumblr/" . $localFile . "\" data-photobooth=\"v2.1\"",
								$text);
			$photoIndex++;
		}
	}
	
	// Convert tumblr blog links
	$text = preg_replace(array("/class=\"tumblr_blog\"/", "/<a ([^>]*?)href=([^>]*?)>/"),
		array("class=\"tumblrBlog\"", "<a $1target=\"_blank\" href=$2>"), $text);
		
	// We're done
	return $text;
}

echo "<pre>";
foreach ($posts->response->posts as $post)
{
	if ($database->querySingle("SELECT count(*) FROM socialMediaPosts WHERE site='tumblr' AND remoteID='" .
		$database->escapeString($post->id) . "'") > 0)
	{
		continue;
	}
	
    $postFiles = array();
	if ($post->type == "photo")
	{
		if (property_exists($post, "photoset_layout"))
		{
			$layout = str_split($post->photoset_layout);
		}
		else
		{
			$layout = array_fill(0, count($post->photos), "1");
		}
		$currentLayoutRow = array_shift($layout);
		$currentRowCount = 0;
		$postText = "<div>";
		$index = 0;
		foreach ($post->photos as $photo)
		{
			if ($currentRowCount == $currentLayoutRow)
			{
				$currentLayoutRow = array_shift($layout);
				$currentRowCount = 0;
			}
			
            $photoFile = file_get_contents($photo->alt_sizes[0]->url);
            $fileInfo = pathinfo($photo->alt_sizes[0]->url);
			$localFile = $post->id . "_" . $index . "." . $fileInfo["extension"];
			$fileTries = 0;
			while (file_exists(DOCUMENT_ROOT . "/images/tumblr/" . $localFile))
			{
				$fileTries++;
				$localFile = $post->id . "_" . $index . "_" . $fileTries . "." . $fileInfo["extension"];
			}
            if (file_put_contents(DOCUMENT_ROOT . "/images/tumblr/" . $localFile, $photoFile) === false)
            {
                foreach ($postFiles as $postFile)
                {
                    unlink($postFile);
                }
                exit ("Error with moving picture to the image/tumblr directory.");
            }
            $postFiles[] = DOCUMENT_ROOT . "/images/tumblr/" . $localFile;            
			$postText .= "<img src=\"/images/tumblr/" . $localFile .
						 "\" class=\"tumblrPhoto ";
			if ($currentLayoutRow == "1")
			{
				$postText .= "large";
			}
			else if ($currentLayoutRow == "2")
			{
				$postText .= "medium";
			}
			else
			{
				$postText .= "small";
			}
			$postText .= "\" data-photobooth=\"v2.1\" />";
			$currentRowCount++;
			$index++;
		}
		$postText .= "</div>\n";
		
		$postText .= "<div class=\"caption\">\n";
		$caption = prepareText($post->id, $post->caption, $postFiles, $index);
		$postText .= $caption;
		/*$caption = $post->caption;
		if (mb_strlen($caption) > 0)
		{
            $postText .= preg_replace(array("/class=\"tumblr_blog\"/", "/<a ([^>]*?)href=([^>]*?)>/"),
                            array("class=\"tumblrBlog\"", "<a $1target=\"_blank\" href=$2>"), $caption);
		}*/
		$postText .= "</div>\n";
		$consoleTitle = "--no-title";
	}
    else if ($post->type == "text")
    {
        $postText = "";        
		$consoleTitle = "";
        if (mb_strlen($post->title) > 0)
        {
            $postText .= "<h4>" . prepareText($post->id, $post->title, $postFiles) . "</h4>";
            $consoleTitle .= "--title=\"" . escapeConsoleString($post->title) . "\"";
        }
        else
        {
            $consoleTitle .= "--no-title";
        }
		
		$postText .= prepareText($post->id, $post->body, $postFiles);
        /*if (mb_strlen($post->body) > 0)
        {
            $bodyText = $post->body;
            $bodyImageMatches = array();
            if (preg_match_all("/<img ([^>]*?)src=\"([^\"]*)\"/", $bodyText, $bodyImageMatches, PREG_SET_ORDER) > 0)
            {
                $uniqueImages = array();
                foreach ($bodyImageMatches as $image)
                {
                    if (in_array($image[2], $uniqueImages))
                    {
                        continue;
                    }
                    
                    $imageFile = file_get_contents($image[2]);
                    $fileInfo = pathinfo($image[2]);
                    do
                    {
                        $localFile = rand() . "." . $fileInfo["extension"];
                    } while (file_exists(DOCUMENT_ROOT . "/images/tumblr/" . $localFile));
                    if (file_put_contents(DOCUMENT_ROOT . "/images/tumblr/" . $localFile, $imageFile) === false)
                    {
                        foreach ($postFiles as $postFile)
                        {
                            unlink($postFile);
                        }
                        exit ("Error with moving picture to the image/tumblr directory.");
                    }
                    
                    $postFiles[] = DOCUMENT_ROOT . "/images/tumblr/" . $localFile;  
                    $uniqueImages[] = $image[2];
                    $bodyText = str_replace($image[2], "/images/tumblr/" . $localFile, $bodyText);
                }
            }
            $postText .= $bodyText;
        }*/
        
        if (mb_strlen($postText) == 0)
        {
            echo "Invalid 'text' element -- resulted in an empty post. Skipped.\n";
            continue;
        }
        
        /*$postText = preg_replace(array("/class=\"tumblr_blog\"/", "/<a ([^>]*?)href=([^>]*?)>/"),
            array("class=\"tumblrBlog\"", "<a $1target=\"_blank\" href=$2>"), $postText);*/
    }
	else if ($post->type == "audio")
    {
        $audioUrl = explode("/", $post->audio_url);
        $audioFile = file_get_contents("http://a.tumblr.com/" . $audioUrl[count($audioUrl) - 1] . "o1.mp3");
        do
        {
            $localFile = rand() . ".mp3";
        } while (file_exists(DOCUMENT_ROOT . "/audio/tumblr/" . $localFile));
        if (file_put_contents(DOCUMENT_ROOT . "/audio/tumblr/" . $localFile, $audioFile) === false)
        {
            exit ("Error with moving audio to the audio/tumblr directory.");
        }
        $postFiles[] = DOCUMENT_ROOT . "/audio/tumblr/" . $localFile;  
        
        $postText = "<audio controls><source src=\"/audio/tumblr/" . $localFile . "\" type=\"" .
            "audio/mp3\" />Your browser doesn't support the <a href=\"" .
            "http://en.wikipedia.org/wiki/HTML5\" target=\"_blank\">HTML5</a> &lt;audio&gt; tag...</audio>";
            
        if (mb_strlen($post->album_art) > 0)
        {
            $albumCover = file_get_contents($post->album_art);
            $fileInfo = pathinfo($post->album_art);
            do
            {
                $localFile = rand() . "." . $fileInfo["extension"];
            } while (file_exists(DOCUMENT_ROOT . "/images/tumblr/" . $localFile));
            if (file_put_contents(DOCUMENT_ROOT . "/images/tumblr/" . $localFile, $albumCover) === false)
            {                
                foreach ($postFiles as $postFile)
                {
                    unlink($postFile);
                }
                exit ("Error with moving album cover to the image/tumblr directory.");
            }
            $postFiles[] = DOCUMENT_ROOT . "/images/tumblr/" . $localFile;
            $postText = "<img src=\"/images/tumblr/" . $localFile . "\" class=\"albumCover\" />" . $postText;
        }
        
        $id3Section = false;
        $titlePieces = array();
        if (mb_strlen($post->track_name) > 0)
        {
            if (!$id3Section)
            {
                $id3Section = true;
                $postText .= "<table class=\"trackInfo\">";
            }
            $postText .= "<tr><td>Track Name:</td><td>" . $post->track_name . "</td></tr>";
            $titlePieces[] = "--title=\"" . escapeConsoleString($post->track_name) . "\"";
        }
        if (mb_strlen($post->artist) > 0)
        {
            if (!$id3Section)
            {
                $id3Section = true;
                $postText .= "<table class=\"trackInfo\">";
            }
            $postText .= "<tr><td>Artist:</td><td>" . $post->artist . "</td></tr>";
            $titlePieces[] = "--artist=\"" . escapeConsoleString($post->artist) . "\"";
        }
        if (mb_strlen($post->album) > 0)
        {
            if (!$id3Section)
            {
                $id3Section = true;
                $postText .= "<table class=\"trackInfo\">";
            }
            $postText .= "<tr><td>Album:</td><td>" . $post->album . "</td></tr>";
            $titlePieces[] = "--album=\"" . escapeConsoleString($post->album) . "\"";
        }
        if ($id3Section)
        {
            $postText .= "</table>";
        }
        $consoleTitle = implode(" ", $titlePieces);
        if (mb_strlen($consoleTitle) == 0)
        {
            $consoleTitle = "--no-info";
        }
        
        $postText .= "<div class=\"audioBreak\"></div>";
        
        if (mb_strlen($post->caption) > 0)
        {
            $bodyText = $post->caption;
            $bodyImageMatches = array();
            if (preg_match_all("/<img ([^>]*?)src=\"([^\"]*)\"/", $bodyText, $bodyImageMatches, PREG_SET_ORDER) > 0)
            {
                $uniqueImages = array();
                foreach ($bodyImageMatches as $image)
                {
                    if (in_array($image[2], $uniqueImages))
                    {
                        continue;
                    }
                    
                    $imageFile = file_get_contents($image[2]);
                    $fileInfo = pathinfo($image[2]);
                    do
                    {
                        $localFile = rand() . "." . $fileInfo["extension"];
                    } while (file_exists(DOCUMENT_ROOT . "/images/tumblr/" . $localFile));
                    if (file_put_contents(DOCUMENT_ROOT . "/images/tumblr/" . $localFile, $imageFile) === false)
                    {
                        foreach ($postFiles as $postFile)
                        {
                            unlink($postFile);
                        }
                        exit ("Error with moving picture to the image/tumblr directory.");
                    }
                    
                    $postFiles[] = DOCUMENT_ROOT . "/images/tumblr/" . $localFile;  
                    $uniqueImages[] = $image[2];
                    $bodyText = str_replace($image[2], "/images/tumblr/" . $localFile, $bodyText);
                }
            }
            $postText .= $bodyText;
        }
        
        $postText = preg_replace(array("/class=\"tumblr_blog\"/", "/<a ([^>]*?)href=([^>]*?)>/"),
            array("class=\"tumblrBlog\"", "<a $1target=\"_blank\" href=$2>"), $postText);
    }
    else if ($post->type == "chat")
    {
        $postText = "";
        $uniqueSpeakers = array();
        foreach ($post->dialogue as $message)
        {
            $postText .= "<div class=\"chatLabel\">" . $message->label . "</div><div class=\"chatMessage\">" .
                $message->phrase . "</div>";
            if (!in_array($message->name, $uniqueSpeakers))
            {
                $uniqueSpeakers[] = $message->name;
            }
        }
        
        $consoleTitle = "--speakers " . count($uniqueSpeakers);
        foreach ($uniqueSpeakers as $speaker)
        {
            $consoleTitle .= " \"" . escapeConsoleString($speaker) . "\"";
        }
        
        $postText = preg_replace(array("/class=\"tumblr_blog\"/", "/<a ([^>]*?)href=([^>]*?)>/"),
            array("class=\"tumblrBlog\"", "<a $1target=\"_blank\" href=$2>"), $postText);
    }
    else
	{
		echo "Post type of '" . $post->type . "' not defined.\n";
		continue;
	}
	
	if (count($post->tags) > 0)
	{
		$postText .= "<div class=\"tumblrTags\">";
		foreach ($post->tags as $tag)
		{
			$postText .= "<a href=\"http://ahlec-si.tumblr.com/tagged/" . 
                preg_replace(array("/\s/", "/\"/"), array("-", "\\\""), $tag) . "/\" target=\"_blank\">" . $tag .
                "</a>\n";
		}
		$postText .= "</div>";
	}
					
	if ($database->exec("INSERT INTO socialMediaPosts(`site`,`avatar`,`dateTime`,`fullText`,`siteURL`,`remoteID`,`isHTML`,`postVerb`) " .
		"VALUES('tumblr','" . $database->escapeString($avatarID) . "','" . date("Y-m-d H:i:s", $post->timestamp) .
		"','" . $database->escapeString($postText) . "','" . $database->escapeString($post->post_url) . "','" .
        $database->escapeString($post->id) . "','1','" . ($isReblog ? "Reblogged" : "Posted") . "')"))
	{
		$isReblog = isset($post->reblogged_from_name);
        $postID = $database->getLastAutoInc();
		
		$consoleCommand = ($isReblog ? "REBLOG" : "POST") . " --type \"" . escapeConsoleString($post->type) . "\" ";
		if ($isReblog)
		{
			$consoleCommand .= "--from \"" . escapeConsoleString($post->reblogged_from_name) . "\" ";
		}
		$consoleCommand .= $consoleTitle;
		
        if (!$database->exec("INSERT INTO console(`directory`,`command`,`dateTime`,`isSocialMedia`,`socialMediaPostID`) VALUES('/remote/tumblr/', '" .
            $database->escapeString($consoleCommand) . "','" . date("Y-m-d H:i:s", $post->timestamp) .
            "','1','" . $database->escapeString($postID) . "')"))
        {
            $database->exec("DELETE FROM socialMediaPosts WHERE postID='" . $database->escapeString($postID) . "'");
            foreach ($postFiles as $postFile)
            {
                unlink($postFile);
            }
        }
	}
}
?>