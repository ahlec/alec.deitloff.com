<?php
require_once ("../lib/config.inc.php");
require_once (DOCUMENT_ROOT . "/crons/tmhOAuth.php");
require_once (DOCUMENT_ROOT . "/crons/tmhUtilities.php");

$tmhOAuth = new tmhOAuth(array(
  'consumer_key'    => 'g4yy4YyLuuINBQ7pcLi2pA',
  'consumer_secret' => 'IKTzArLonrHYb6aubfR7zqe3fvbgGZnbabK033gkM',
  'user_token'      => '116247548-zngvQJDYxhr70Up4zMqL78zA6KEKjeU2w09OhdiS',
  'user_secret'     => 'nq4JI6WpopzYiCjwm7qLrVfz5D0DAMhToeHLd5qfe0',
));
//$tmhOAuth->request('GET', 'http://search.twitter.com/search.json', array('q' => 'from:jyavoc', 'since_id' => '', 'rpp' => '', 'geocode' => '',
//    'lang' => '', 'include_entities' => 'true'), false);
    
$tmhOAuth->request("GET", "http://api.twitter.com/1/statuses/user_timeline.json", array("screen_name" => "jyavoc", "include_rts" => "true",
    "include_entities" => "true"), false);
    
if ($tmhOAuth->response['code'] == 200)
{
    $results = json_decode($tmhOAuth->response["response"], true);
    //$data = json_decode($tmhOAuth->response, true);//$tmhOAuth->response['response'], true);
    /*foreach ($data['results'] as $tweet)
    {
        $results[$tweet['id_str']] = $tweet;
    }*/
}
else
{
    exit('There was an error.');
}

//print_r($results);
//exit();

$database = database();

function sortByIndices($entityA, $entityB)
{
    if ($entityA["indices"][0] == $entityB["indices"][0])
    {
        return 0;
    }
    return ($entityA["indices"][0] > $entityB["indices"][0] ? -1 : 1);
}

$avatars = array();
foreach ($results as $result)
{
    //if (isset($result["to_user"]) && $result["to_user"] != null)
    if ($result["in_reply_to_user_id_str"] != null)
    {
        continue;
    }
    
    $postFiles = array();
    
    if (isset($result["retweeted_status"]))
    {
        $fullText = $result["retweeted_status"]["text"];
        $rawText = $result["retweeted_status"]["text"];
        $entities = $result["retweeted_status"]["entities"];
        
        $originalTweetAvatarName = end(explode("/", $result["retweeted_status"]["user"]["profile_image_url"]));
        $originalTweetAvatarName = str_replace("_normal", "", $originalTweetAvatarName);
        
        if (array_key_exists($originalTweetAvatarName, $avatars))
        {
            $avatarID = $avatars[$originalTweetAvatarName];
        }
        else
        {
            $avatar = file_get_contents(str_replace("_normal", "", $result["retweeted_status"]["user"]["profile_image_url"]));
            $avatarHash = md5($avatar);
            if ($database->querySingle("SELECT count(*) FROM socialMediaAvatars WHERE site='twitter' AND hash='" . $database->escapeString($avatarHash) .
                "'") > 0)
            {
                $avatarID = $database->querySingle("SELECT avatarID FROM socialMediaAvatars WHERE site='twitter' AND hash='" .
                    $database->escapeString($avatarHash) . "' LIMIT 1");
            }
            else
            {
                do
                {
                    $localFile = "twitter" . rand() . ".jpg";
                } while (file_exists(DOCUMENT_ROOT . "/images/avatars/" . $localFile));
                if (file_put_contents(DOCUMENT_ROOT . "/images/avatars/" . $localFile, $avatar) === false)
                {
                    exit ("Error with moving new avatar to the avatar directory.");
                }
                
                if ($database->exec("INSERT INTO socialMediaAvatars(`site`,`localFile`,`hash`,`overlay`) VALUES('twitter','" .
                    $database->escapeString($localFile) . "','" . $database->escapeString($avatarHash) . "','retweet.png')"))
                {
                    $avatarID = $database->getLastAutoInc();
                }
                else
                {
                    unlink(DOCUMENT_ROOT . "/images/avatars/" . $localFile);
                    exit ("Error with establishing a new avatar.");
                }
            }
            
            $avatars[$originalTweetAvatarName] = $avatarID;
        }
        
    }
    else
    {
        $fullText = $result["text"];
        $rawText = $result["text"];
        $entities = $result["entities"];
        
        $remoteAvatarName = end(explode("/", $result["user"]["profile_image_url"]));
        $remoteAvatarName = str_replace("_normal", "", $remoteAvatarName);
        
        if (array_key_exists($remoteAvatarName, $avatars))
        {
            $avatarID = $avatars[$remoteAvatarName];
        }
        else
        {
            $avatar = file_get_contents(str_replace("_normal", "", $result["user"]["profile_image_url"]));
            $avatarHash = md5($avatar);
            if ($database->querySingle("SELECT count(*) FROM socialMediaAvatars WHERE site='twitter' AND hash='" . $database->escapeString($avatarHash) .
                "'") > 0)
            {
                $avatarID = $database->querySingle("SELECT avatarID FROM socialMediaAvatars WHERE site='twitter' AND hash='" .
                    $database->escapeString($avatarHash) . "' LIMIT 1");
            }
            else
            {
                do
                {
                    $localFile = "twitter" . rand() . ".jpg";
                } while (file_exists(DOCUMENT_ROOT . "/images/avatars/" . $localFile));
                if (file_put_contents(DOCUMENT_ROOT . "/images/avatars/" . $localFile, $avatar) === false)
                {
                    exit ("Error with moving new avatar to the avatar directory.");
                }
                
                if ($database->exec("INSERT INTO socialMediaAvatars(`site`,`localFile`,`hash`) VALUES('twitter','" .
                    $database->escapeString($localFile) . "','" . $database->escapeString($avatarHash) . "')"))
                {
                    $avatarID = $database->getLastAutoInc();
                }
                else
                {
                    unlink(DOCUMENT_ROOT . "/images/avatars/" . $localFile);
                    exit ("Error with establishing a new avatar.");
                }
            }
            
            $avatars[$remoteAvatarName] = $avatarID;
        }
    }
    
    $rawText = str_replace("\n", "\\n", $rawText);
    
    $isHTML = false;
    if (preg_match("/(^|\s)#(\pL+)/", $fullText) === 1 || preg_match("/@([A-Za-z0-9_]+)/", $fullText) === 1)
    {
        $fullText = preg_replace("/(^|\s)#(\pL+)/",
            "$1<a href=\"http://www.twitter.com/search?q=%23$2\" target=\"_blank\">#$2</a>",
            preg_replace("/@([A-Za-z0-9_]+)/", "<a href=\"http://www.twitter.com/$1\" target=\"_blank\">@$1</a>",
            $fullText));
        $isHTML = true;
    }
    
    if (count($entities["urls"]) > 0)
    {
        $urls = $entities["urls"];
        usort($urls, "sortByIndices");
        
        foreach ($urls as $url)
        {
            $fullText = str_replace($url["url"], "<a href=\"" . escapeConsoleString($url["expanded_url"]) . "\" target=\"_blank\">" .
                escapeConsoleString($url["display_url"]) . "</a>", $fullText);
        }
        $isHTML = true;
    }
    
    if (isset($entities["media"]) && count($entities["media"]) > 0)
    {
		foreach ($entities["media"] as $media)
		{
            if ($media["type"] != "photo")
            {
                echo "media type '" . $media["type"] . "' not supported yet. skipping.\n";
                continue;
            }
            
            $photoFile = file_get_contents($media["media_url"]);
            $fileInfo = pathinfo($media["media_url"]);
            do
            {
                $localFile = rand() . "." . $fileInfo["extension"];
            } while (file_exists(DOCUMENT_ROOT . "/images/twitter/" . $localFile));
            if (file_put_contents(DOCUMENT_ROOT . "/images/twitter/" . $localFile, $photoFile) === false)
            {
                foreach ($postFiles as $postFile)
                {
                    unlink($postFile);
                }
                exit ("Error with moving picture to the image/twitter directory.");
            }
            $postFiles[] = DOCUMENT_ROOT . "/images/twitter/" . $localFile;

            $fullText = str_replace($media["url"], "<img src=\"/images/twitter/" . $localFile . "\" class=\"twitterPhoto\" onclick=\"photobooth('/images/twitter/" .
                $localFile . "');\" />", $fullText);
            $isHTML = true;
		}
    }
    
    $post = array("avatar" => $avatarID,
        "dateTime" => date("Y-m-d H:i:s", strtotime($result["created_at"]) - 14400),
        "rawText" => $rawText,
        "fullText" =>  $fullText, 
        "remoteID" => $result["id_str"],
        "isHTML" => $isHTML,
        "isRetweet" => (isset($result["retweeted_status"])),
        "originalUser" => (isset($result["retweeted_status"]) ? $result["retweeted_status"]["user"]["screen_name"] : $result["user"]["screen_name"]));
    
    if ($database->querySingle("SELECT count(*) FROM socialMediaPosts WHERE `site`='twitter' AND remoteID='" .
        $database->escapeString($post["remoteID"]) . "'") > 0)
    {
        foreach ($postFiles as $file)
        {
            unlink($file);
        }
        continue;
    }
    
    if ($database->exec("INSERT INTO socialMediaPosts(`site`,`avatar`,`dateTime`,`fullText`,`siteURL`,`remoteID`,`isHTML`,`postVerb`" .
        ($post["isRetweet"] ? ",`alternateAccountURL`,`alternateAccountName`" : "") . ") VALUES('twitter','" .
        $database->escapeString($post["avatar"]) . "','" . $database->escapeString($post["dateTime"]) . "','" .
        $database->escapeString($post["fullText"]) . "','http://www.twitter.com/jyavoc/status/" .
        $database->escapeString($post["remoteID"]) . "','" . $database->escapeString($post["remoteID"]) . "','" .
        ($post["isHTML"] ? "1" : "0") . "','" . ($post["isRetweet"] ? "Retweeted" : "Tweeted") . "'" . ($post["isRetweet"] ? ",'" .
        "http://www.twitter.com/" . $post["originalUser"] . "','@" . $post["originalUser"] . "'" : "") . ")"))
    {
        $postID = $database->getLastAutoInc();
        if (!$database->exec("INSERT INTO console(`directory`,`command`,`dateTime`,`isSocialMedia`,`socialMediaPostID`) VALUES('/remote/twitter/', '" .
            $database->escapeString(($post["isRetweet"] ? "RETWEET @" . $post["originalUser"] . ":" : "TWEET") ." \"" .
                escapeConsoleString($post["rawText"]) . "\"") . "','" . $database->escapeString($post["dateTime"]) .
                "','1','" . $database->escapeString($postID) . "')"))
        {
            $database->exec("DELETE FROM socialMediaPosts WHERE postID='" . $database->escapeString($postID) . "'");
            foreach ($postFiles as $file)
            {
                unlink($file);
            }
        }
    }
    else
    {
        foreach ($postFiles as $file)
        {
            unlink($file);
        }
    }
    
    var_dump($post);
}