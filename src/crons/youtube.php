<?php
require_once ("../lib/config.inc.php");
require_once (DOCUMENT_ROOT . "/crons/Zend/Loader.php");
$database = database();
Zend_Loader::loadClass('Zend_Gdata_YouTube');
Zend_Loader::loadClass('Zend_Gdata_ClientLogin');

function convert($str,$ky=''){ 
if($ky=='')return $str; 
$ky=str_replace(chr(32),'',$ky); 
if(strlen($ky)<8)exit('key error'); 
$kl=strlen($ky)<32?strlen($ky):32; 
$k=array();for($i=0;$i<$kl;$i++){ 
$k[$i]=ord($ky{$i})&0x1F;} 
$j=0;for($i=0;$i<strlen($str);$i++){ 
$e=ord($str{$i}); 
$str{$i}=$e&0xE0?chr($e^$k[$j]):chr($e); 
$j++;$j=$j==$kl?0:$j;} 
return $str; 
}

$httpClient = Zend_Gdata_ClientLogin::getHttpClient("jacob.deitloff@gmail.com", convert("BwfniaGu~xfq! ",
    "deitloffj0t00wbK3nn7wur7"), "youtube");

$yt = new Zend_Gdata_YouTube($httpClient, "", "",
    "AI39si6Yt10n6R4Rn1hMKJXvSpCIiGhB50P5dOI_xT5tYzzsS-ct-gkIoRJikSseoN8tyoITWSSb0wfMxTrj9sUoAZqOh66PiQ");
$yt->setMajorProtocolVersion(2);
$userProfileEntry = $yt->getUserProfile("JacobOfElsewhere");

$avatar = file_get_contents($userProfileEntry->getThumbnail()->getUrl());
$avatarHash = md5($avatar);
if ($database->querySingle("SELECT count(*) FROM socialMediaAvatars WHERE site='youtube' AND hash='" .
    $database->escapeString($avatarHash) . "'") > 0)
{
    $avatarID = $database->querySingle("SELECT avatarID FROM socialMediaAvatars WHERE site='youtube' AND hash='" .
        $database->escapeString($avatarHash) . "' LIMIT 1");
}
else
{
    do
    {
        $localFile = "youtube" . rand() . ".jpg";
    } while (file_exists(DOCUMENT_ROOT . "/images/avatars/" . $localFile));
    if (file_put_contents(DOCUMENT_ROOT . "/images/avatars/" . $localFile, $avatar) === false)
    {
        exit ("Error with moving new avatar to the avatar directory.");
    }
    
    if ($database->exec("INSERT INTO socialMediaAvatars(`site`,`localFile`,`hash`) VALUES('youtube','" .
        $database->escapeString($localFile) . "','" . $database->escapeString($avatarHash) . "')"))
    {
        $avatarID = $database->getLastAutoInc();
    }
    else
    {
        unlink(DOCUMENT_ROOT . "/images/avatars/" . $localFile);
        exit ("Error with establishing a new avatar.");
    }
}

$favorites = $yt->getUserFavorites("JacobOfElsewhere");
foreach ($favorites as $favorite)
{
    if ($database->querySingle("SELECT count(*) FROM socialMediaPosts WHERE site='youtube' AND remoteID='" .
        $database->escapeString($favorite->getVideoId()) . "'") > 0)
    {
        continue;
    }
    
    $postText = "<a href=\"http://www.youtube.com/user/JacobOfElsewhere\" target=\"_blank\">JacobOfElsewhere</a> favourited a video:\n" .
        "<iframe width=\"560\" height=\"315\" src=\"http://www.youtube.com/embed/" . 
        $favorite->getVideoId() . "\" frameborder=\"0\" allowfullscreen></iframe>";
    if ($database->exec("INSERT INTO socialMediaPosts(`site`,`avatar`,`dateTime`,`fullText`,`siteURL`,`remoteID`,`isHTML`) " .
        "VALUES('youtube','" . $database->escapeString($avatarID) . "','" . date("Y-m-d H:i:s",
        strtotime($favorite->getUpdated()->getText())) . "','" . $database->escapeString($postText) .
        "','http://www.youtube.com/watch?v=" . $database->escapeString($favorite->getVideoId()) . "','" .
        $database->escapeString($favorite->getVideoId()) . "','1')"))
    {        
        $postID = $database->getLastAutoInc();
        $database->exec("INSERT INTO console(`directory`,`command`,`dateTime`,`isSocialMedia`,`socialMediaPostID`) " .
            "VALUES('/remote/youtube/','FAVOURITE \"" .
            $database->escapeString(escapeConsoleString($favorite->getVideoTitle())) . "\"','" .
            $database->escapeString(date("Y-m-d H:i:s", strtotime($favorite->getUpdated()->getText()))) .
            "','1','" . $database->escapeString($postID) . "')");
    }
    else
    {
        echo "Problem with adding \"" . $favorite->getVideoTitle() . "\"\n";
    }
}
?>