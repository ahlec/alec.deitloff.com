<?php
error_reporting(E_ALL);
ini_set("display_errors", "1");
ini_set("default_charset", "utf-8");
ini_set("date.timezone", "America/New_York");

define("DOCUMENT_ROOT", "/home/deitloff/public_html/alec");
define("WEB_ROOT", "http://alec.deitloff.com");
define("BASE_DOCUMENT_ROOT", "/home/deitloff/public_html");
define("BASE_WEB_ROOT", "http://www.deitloff.com");
define("DEFAULT_SKIN", "gravitySuit");

define("DATE_FORMAT", "j F Y");
define("WEEKDAY_DATE_FORMAT", "l, j F Y");
define("TIME_FORMAT", "g:i A");
define("DATETIME_FORMAT", "l, j F Y \a\\t g:i A");
define("DAYMONTH_FORMAT", "j F");
define("NUMBER_CONSOLE_COMMANDS", 10);

define("SUGGESTION_COLUMN", "alec.deitloff.suggestions_col");
define("SUGGESTION_DIRECTION", "alec.deitloff.suggestions_dir");
define("ASK_COLUMN", "alec.deitloff.ask_col");
define("ASK_DIRECTION", "alec.deitloff.ask_dir");
define("ASK_LAST_POST_ID", "alec.deitloff.ask_last_post");

define ("MAX_ASK_VOTES", 50);


define("FACEBOOK_APP_ID", "120257234693722");
define("FACEBOOK_APP_SECRET_ID", "13c407cbdf32e55f299b191d4353ed59");


// brilliant:
// http://stackoverflow.com/questions/1732348/regex-match-open-tags-except-xhtml-self-contained-tags/1732454#1732454

/* database */
require_once ("/home/deitloff/core.inc.php");
function database()
{
    $database = new VersatileDatabase("localhost", "deitloff_main", "md%7nfF@Z(H(", "deitloff_alec");
    if ($database->valid())
    {
        return $database;
    }
    return null;
}

@session_start();

/* console */
function escapeConsoleString($str)
{
    return htmlentities(preg_replace(array("/\"/", "/\\\\/"), array("\\\"", "\\\\"), $str), ENT_COMPAT, "UTF-8");
}
?>
