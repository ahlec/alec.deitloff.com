<?php
class VersatileDatabase
{
  private $_database;
  function __construct($server, $username, $password, $database)
  {
    $this->_database = @mysql_connect($server, $username, $password);
    if ($this->_database === false)
    {
        return;
    }
    mysql_select_db($database, $this->_database);
    $this->exec("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");
	$this->exec("PRAGMA short_column_names = ON");
	$this->exec("PRAGMA encoding = \"UTF-8\"");
  }
    function valid()
    {
        return ($this->_database !== false);
    }
  function query($query)
  {
    $results = mysql_query($query, $this->_database);
    return ($results == FALSE ? FALSE : new DeitloffDatabaseResult($results));
  }
    function querySingle($query, $returnFullRow = false)
    {
        $results = mysql_query($query, $this->_database);
        if (is_bool($results))
        {
            $error_no = mysql_errno($this->_database);
            if ($error_no === 0)
            {
                return $results;
            }
            
            trigger_error("MySQL Error #" . $error_no . ". " . mysql_error($this->_database), E_USER_ERROR);
        }
        $resultArray = mysql_fetch_assoc($results);
        if ($returnFullRow)
        {
            return $resultArray;
        }
        if (is_bool($resultArray))
        {
            return $resultArray;
        }
        return array_shift($resultArray);
    }
    function exec($query)
    {
        return mysql_query($query, $this->_database);
    }
  function escapeString($string)
  {
    return mysql_real_escape_string($string, $this->_database);
  }
  function close()
  {
    mysql_close($this->_database);
  }
  function lastErrorCode()
  {
    return mysql_errno($this->_database);
  }
  function lastErrorMsg()
  {
    return mysql_error($this->_database);
  }
  function getLastAutoInc()
  {
    return mysql_insert_id($this->_database);
  }
}
class DeitloffDatabaseResult
{
  private $_results;
  function __construct($results)
  {
	$this->_results = $results;
  }
  function fetchArray()
  {
    return mysql_fetch_array($this->_results, MYSQL_ASSOC);
  }
  function reset()
  {
    return mysql_data_seek($this->_results, 0);
  }
  function numberRows()
  {
    return mysql_num_rows($this->_results);
  }
}
?>
