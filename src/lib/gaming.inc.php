<?php
function getConsoleName($consoleHandle)
{
    switch ($consoleHandle)
    {
        // Nintendo
        case "gba": return "GameBoy Advance";
        case "gbc": return "GameBoy Color";
        case "gamecube": return "GameCube";
        case "n64": return "Nintendo 64";
        case "nds": return "Nintendo DS";
        case "3ds": return "Nintendo 3DS";
        case "wii": return "Nintendo Wii";
        case "nes": return "NES";
        case "snes": return "SNES";
        
        // Microsoft
        case "xbox": return "Xbox";
        case "xbox360": return "Xbox 360";
        
        // Sony
        case "ps1": return "PlayStation";
        case "ps2": return "PlayStation 2";
        case "ps3": return "PlayStation 3";
        case "psp": return "PlayStation Portable";
        
        // Computer
        case "computer": return "Desktop";
    }
    
    trigger_error("Unknown console handle ('" . $consoleHandle . "') provided to getConsoleName().");
    return $consoleHandle;
}
?>