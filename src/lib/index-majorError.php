<?php
echo "Holy crap! Something <strong>MAJOR</strong> happened. There's been a huge explosion somewhere, and the site is <i>completely</i> inaccessible!<br /><br />Please bear with me in this moment of vulnerability. We shall get through this together.<br /><br />&mdash; Jacob Deitloff";
?>