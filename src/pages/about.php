﻿<?php
class Page
{
    function preRender($database, $arguments)
    {
    }
    function outputColumn($database, $arguments)
	{
        echo "<div><strong>Full name</strong> Alec Jacob Deitloff</div>";
        //echo utf8_encode("<div><strong>Sÿe name</strong> Älec es Îbirz sî Dÿtlof</div>");
        echo "<div><strong>Sÿe name</strong> Älec es Îbirz sî Dÿtlof</div>";
        echo "<div><strong>Birthday</strong> 26 October 1992 (Age " . (date("Y") - (date("m-d") >= "10-26" ?
            0 : 1) - 1992) . ")</div>";
        echo "<div><strong>University studies</strong> Computer Science and German language, University of Pittsburgh</div>";
        echo "<div><strong>Languages spoken</strong> <ul class=\"languages\"><li class=\"en\">English</li><li " .
            "class=\"de\">German</li><li class=\"jp\">Japanese</li></ul></div>";
	}
    function output($database, $arguments)
    {
        echo "<h1>A short autobiography</h1>\n";
        echo $database->querySingle("SELECT contents FROM textLibrary WHERE handle='autobiography' LIMIT 1");
        echo "<h1>Alec answers questions</h1>\n";
        echo "<div class=\"forewarning\">But do you think you're prepared for it?</div>\n";
        $boxes = $database->query("SELECT `column`, width, question, about.answer, centerText FROM aboutBoxes JOIN about ON " .
            "aboutBoxes.answer = about.aboutID ORDER BY sortHeight ASC, (CASE `column` WHEN 'right' THEN 1 WHEN 'left' THEN 2 ELSE 3 END) ASC");
        while ($box = $boxes->fetchArray())
        {
            if ($box["width"] == "three")
            {
                echo "<div class=\"aboutBoxThreeProcession\"></div>\n";
            }
            echo "<div class=\"aboutBox " . $box["width"] . " " . $box["column"] . ($box["centerText"] == "1" ? " centerText" : "") . "\">\n";
            echo "  <div class=\"question\"><span>" . $box["question"] . "</span></div>\n";
            echo "  <div class=\"answer\">" . $box["answer"] . "</div>\n";
            echo "</div>\n";
        }
        echo "<div class=\"askPromo\"><a href=\"/about/ask/\">You can ask me more questions by clicking here!</a></div>\n";
    }
}
?>
