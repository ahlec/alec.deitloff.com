<?php
class Page
{
    private $_attemptedAsk = false;
    private $_askResponses = array();
    private $_sortColumn = "IF(status='answered', 1, IF(status='pending', 2, 3)) ASC, votes";
    private $_sortColumnName = "status";
    private $_sortDirection = "DESC";
    
    function preRender($database, $arguments)
    {
        if (isset($_POST["question"]) && mb_strlen($_POST["question"]) > 0 && isset($_POST["postID"]) && (!isset($_SESSION[ASK_LAST_POST_ID]) ||
            $_SESSION[ASK_LAST_POST_ID] != $_POST["postID"]))
        {
            $this->_attemptedAsk = true;
            $_SESSION[ASK_LAST_POST_ID] = $_POST["postID"];
            $uniqueQuestions = explode("\r\n", htmlentities($_POST["question"]));            
            foreach ($uniqueQuestions as $question)
            {
                if (mb_strlen($question) == 0 || ctype_space($question))
                {
                    continue;
                }
                
                $firstDuplicate = $database->querySingle("SELECT askId, question, status, votes FROM ask WHERE LOWER(question) = '" .
                    $database->escapeString(mb_strtolower($question)) . "' LIMIT 1", true);
                if ($firstDuplicate != null)
                {
                    if (($firstDuplicate["status"] == "pending" || $firstDuplicate["status"] == "noLongerApplicable") && $firstDuplicate["votes"] < MAX_ASK_VOTES)
                    {
                        if ($database->exec("UPDATE ask SET votes = votes + 1 WHERE askId='" . $database->escapeString($firstDuplicate["askId"]) . "'"))
                        {
                            $this->_askResponses[] = array("success", "Voted for question '<strong>" . $firstDuplicate["question"] . "</strong>'.",
                                "Thanks for it!");
                        }
                        else
                        {
                            $this->_askResponses[] = array("failure", "Could not tally vote for question '<strong>" . $firstDuplicate["question"] . "</strong>'.",
                                "Please try again.");
                        }
                    }
                    else if ($firstDuplicate["status"] == "answered")
                    {
                        $this->_askResponses[] = array("failure", "The question '<strong>" . $firstDuplicate["question"] . "</strong>' was already answered!",
                            "No need to ask it again, right?");
                    }
                    else if ($firstDuplicate["status"] == "rejected")
                    {
                        $this->_askResponses[] = array("failure", "The question '<strong>" . $firstDuplicate["question"] . "</strong>' was already rejected!",
                            "I refuse to answer this question. Please respect that.");
                    }
                    else if ($firstDuplicate["votes"] >= MAX_ASK_VOTES)
                    {
                        $this->_askResponses[] = array("failure", "The question '<strong>" . $firstDuplicate["question"] . "</strong>' already has enough votes!",
                            "I got the message, though.");
                        if ($firstDuplicate["status"] == "noLongerApplicable")
                        {
                            $database->exec("UPDATE ask SET status='pending' WHERE askId='" . $database->escapeString($firstDuplicate["askId"]) . "'");
                        }
                    }
                    
                    continue;
                }
                
                if ($database->exec("INSERT INTO ask(question, status, dateAsked, votes) VALUES('" . $database->escapeString($question) . "','pending','" .
                    date("Y-m-d H:i:s") . "', 1)"))
                {
                    $this->_askResponses[] = array("success", "The question '<strong>" . $question . "</strong>' was submitted!", "Thank you so much :)");
                }
                else
                {
                    $this->_askResponses[] = array("failure", "Could not submit '<strong>" . $question . "</strong>'!", "Perhaps you could try again?");
                }
            }
        }
        
	if (isset($_POST["suggestion"]) && mb_strlen($_POST["suggestion"]) > 0)
	{
		$this->_attemptedSuggest = true;
		$this->_suggestSuccess = $database->exec("INSERT INTO suggestionsQueue(input, submitted) VALUES('" .
			$database->escapeString($_POST["suggestion"]) . "','" .
			date("Y-m-d H:i:s") . "')");
	}

    
        if (isset($_SESSION[ASK_DIRECTION]) && $_SESSION[ASK_DIRECTION] == "ASC")
        {
            $this->_sortDirection = "ASC";
        }
        if (isset($_SESSION[ASK_COLUMN]) && $_SESSION[ASK_COLUMN] == "question")
        {
            $this->_sortColumn = "question";
            $this->_sortColumnName = "question";
        }
    }
    
    function outputColumn($database, $arguments)
	{
        echo "<div><strong>And this...?</strong> Well, I think it's kind of fun answering questions. Most websites have a " .
            "\"Frequently Asked Questions\" section, but I don't necessarily <i>have</i> any frequently asked questions. So I thought " .
            "I'd give people a chance to give me some. I look at all of the questions that I'm asked and I choose the ones which are " .
            "both the most informative <i>and</i> the most humourous (either in question or in answer) and I respond to them on my " .
            "<a href=\"/about/\">about me</a> page.</div>\n";
        
        echo "<div><strong>So I can ask you anything?</strong> Well, technically, you <i>could</i> punch me in the face, but you'd " .
            "promptly be arrested. Likewise, you <i>technically</i> can ask me anything, but I'm not guaranteeing that I'll respond " .
            "to it, especially if it falls within one or more of these categories: <ul><li><u>Sexual conquests.</u> Yes, I have " .
            "gotten these questions before.</li><li><u>Inflamatory materials.</u> Racism, sexism, or personal attacks fall into this " .
            "category.</li><li><u>Boring materials.</u> I'm not likely to respond to \"What did you eat for breakfast today?\"</li>" .
            "<li><u>Personal information.</u> By this, I mean home address, mobile number, work e-mail, social security number, " .
            "blood type of my third ex-wife, etc.</li></ul>Naturally, this isn't an exhaustive list, and just because a question you " .
            "ask isn't on this list doesn't mean that I will respond to it. In general, though, if you ask interesting/funny questiosn " .
            "which are appropriate in nature, I'm a very light-hearted individual.</div>\n";
            
        echo "<div><strong>You didn't answer the question I asked!</strong> Yes, it means I hate you. No, I don't want you to contact " .
            "me and pester me that I didn't respond to it. No, I don't want you to resubmit it so that I can see it again, because " .
            "<i>\"perhaps it got lost.\"</i><br /><br />In actuality, though, this form, like all parts of my website, does not record " .
            "your personal information &mdash; only your question. I might know it is you if you ask me about something and I can " .
            "logically figure out it is you, but otherwise, it's anonymous. That said, I really <i>don't</i> want you to contact me " .
            "or resubmit the question. I cannot answer every question that is posed to me, and so I don't mean it as a personal " .
            "rejection, or that I even dislike your question. I just can't answer everything.</div>\n";
	}
    function output($database, $arguments)
    {
        if ($this->_attemptedAsk)
        {
            foreach ($this->_askResponses as $response)
            {
                echo "<div class=\"status " . $response[0] . "\">" . $response[1] . "<div class=\"miniText\">" . $response[2] . "</div></div>";
            }
        }
    
        echo "<h1>Ask me some questions!</h1>\n";
        echo "  <form method=\"post\" action=\"" . WEB_ROOT . "/about/ask/\">\n";
		echo "    <input type=\"text\" name=\"question\" class=\"submissionBox\" />\n";
        echo "    <input type=\"hidden\" name=\"postID\" value=\"" . rand() . "\" />\n";
		echo "    <input type=\"submit\" value=\"Ask it!\" class=\"submissionButton\" />\n";
		echo "  </form>\n";
        
        echo "<h1>Current question queue</h1>\n";        
        $pastQuestions = $database->query("SELECT askId, question, status, votes FROM ask WHERE status<>'noLongerApplicable' ORDER BY " .
            $this->_sortColumn . " " . $this->_sortDirection);
            
        echo "  <table class=\"questions\">\n";
		echo "    <tr class=\"header\">\n";
		echo "      <td id=\"status\"><div class=\"iconWrapper\">Status <img src=\"" . WEB_ROOT .
			"/style/images/" . ($this->_sortDirection == "ASC" && $this->_sortColumnName == "status" ? "asc" : "desc") .
			".png\" class=\"sortIcon" . ($this->_sortColumnName == "status" ? " current" : "") .
			"\" title=\"Reorder\" onclick=\"questionsSort('status');\" /></div></td>\n";
		echo "      <td id=\"question\"><div class=\"iconWrapper\">Question <img src=\"" . WEB_ROOT . "/style/images/" .
			($this->_sortDirection == "ASC" && $this->_sortColumnName == "question" ? "asc" : "desc") .
			".png\" class=\"sortIcon" . ($this->_sortColumnName == "question" ? " current" : "") . "\" title=\"Reorder\" " .
			"onclick=\"questionsSort('question');\" /></div></td>\n";
		echo "    </tr>\n";
        
        while ($question = $pastQuestions->fetchArray())
        {
            echo "<tr" . ($question["status"] == "answered" ? " class=\"completed\"" :
				($question["status"] == "rejected" ? " class=\"rejected\"" : "")) . ">\n";
			echo "  <td><div class=\"voteWrapper\">";
			if ($question["status"] == "pending" && $question["votes"] < MAX_ASK_VOTES)
			{
				echo "<img src=\"" . WEB_ROOT . "/style/images/up_vote.png\" onclick=\"questionVote('" .
					$question["askId"] . "');\" class=\"upVote\" title=\"Vote for this question.\" /> " .
					"<span id=\"votes-" . $question["askId"] . "\">" . $question["votes"] . " <span class=\"label\">vote" .
                    ($question["votes"] != 1 ? "s" : "") . "</span></span>";
			}
			else if ($question["status"] == "answered")
			{
				echo "<img src=\"" . WEB_ROOT .	"/style/images/complete.png\" title=\"Answered\" class=\"statusIcon\" /> Answered";
			}
			else if ($question["status"] == "rejected")
			{
				echo "<img src=\"" . WEB_ROOT .	"/style/images/rejected.png\" title=\"Rejected\" class=\"statusIcon\" /> Rejected";
			}
            else if ($question["status"] == "pending" && $question["votes"] >= MAX_ASK_VOTES)
            {
                echo $question["votes"] . " <span class=\"label\">vote" . ($question["votes"] != 1 ? "s" : "") . "</span>";
            }
			echo "</div></td>\n";
            echo "  <td>" . $question["question"] . "</td>\n";
            echo "</tr>\n";
        }
        
        echo "</table>\n";
    }
}
?>