<?php
class Page
{
    private $_emailQuotes = array(
        array("I'd rather send out a mass email then hang posters all over the place.", "Todd Barry"),
        array("I don't believe in email. I'm an old-fashioned girl. I prefer calling and hanging up.", "Sarah Jessica Parker"),
        array("All the trends show that email usage among the younger cohorts of Internet users is declining. Whether it will take five or 30 years for email to go extinct, I'm not sure.", "Andrew Mason"),
        array("In the time honored tradition of email, just ignore the question.", "John Dobbin"),
        array("The difference between e-mail and regular mail is that computers handle e-mail, and computers never decide to come to work one day and shoot all the other computers.", "Jamais Cascio"),
        array("Anyone wishing to communicate with Americans should do so by e-mail, which has been specially invented for the purpose, involving neither physical proximity nor speech", "Auberon Waugh")
        );
    private $_attemptedContact = false;
    private $_contactSuccess = false;
    private $_responseMessage = null;
    private $_responseSubtext = null;
    
    private $_cachedName = null;
    private $_cachedEmail = null;
    private $_cachedMessage = null;
    function preRender($database, $arguments)
    {
        if (isset($_POST["email"]) && isset($_POST["name"]) && isset($_POST["message"]))
        {
            $this->_attemptedContact = true;
            $this->_cachedName = $_POST["name"];
            $this->_cachedEmail = $_POST["email"];
            $this->_cachedMessage = $_POST["message"];
            
            if (mb_strlen($_POST["name"]) < 2)
            {
                $this->_contactSuccess = false;
                $this->_responseMessage = "Your name is too short!";
                $this->_responseSubtext = "Please give me a reasonable name, so I can tailor a better response!";
                $this->_cachedName = null;
                return;
            }
            
            if (mb_strlen($_POST["message"]) == 0 || ctype_space($_POST["message"]))
            {
                $this->_contactSuccess = false;
                $this->_responseMessage = "Message is too short!";
                $this->_responseSubtext = "Please type me something &mdash; I'd like to have something to read!";
                $this->_cachedMessage = null;
                return;
            }
            
            $userEmail = filter_var($_POST["email"], FILTER_VALIDATE_EMAIL);
            if (mb_strlen($_POST["email"]) == 0 || $userEmail === false)
            {
                $this->_contactSuccess = false;
                $this->_responseMessage = "Please provide a valid e-mail!";
                $this->_responseSubtext = "After all, how would I be able to respond to you otherwise?";
                $this->_cachedEmail = null;
                return;
            }
            
            if (!mail("jacob@deitloff.com", "A message from " . $_POST["name"],
                "'" . $_POST["name"] . "' <" . $userEmail . "> wrote:\n\n" . $_POST["message"],
                "From: jacob@deitloff.com\r\nReply-To: " . $userEmail . "\r\nX-Mailer: PHP/" . phpversion()))
            {
                $this->_contactSuccess = false;
                $this->_responseMessage = "E-mail could not be sent!";
                $this->_responseSubtext = "There seems to have been a problem somewhere. Can you try again for me?";
                return;
            }
            
            $this->_contactSuccess = true;
            $this->_responseMessage = "Your message was sent.";
            $this->_responseSubtext = "Thanks for your message; I can't wait to read it!";
            $this->_cachedName = null;
            $this->_cachedEmail = null;
            $this->_cachedMessage = null;
        }
    }
    function outputColumn($database, $arguments)
	{
        echo "<div><strong>What's this?</strong> Good news is that it allows you to contact me, and allows me to be " .
            "able to respond directly back to you, if the message so requires. However, because the internet can be " .
            "a place of less-than-desirable connivings, I'm not making my contact information directly available to the " .
            "public. It's a common enough tactic &mdash; having a web form that allows the visitor to put in their own " .
            "contact information as well as a message, and then sending that to the recipient. I know that it isn't " .
            "always what you hope to find, but I think it will do the job justice, if you allow it.</div>";
	}
    function output($database, $arguments)
    {
        echo "<h1>Contact me</h1>\n";
        if ($this->_attemptedContact)
		{
			echo "<div class=\"status " . ($this->_contactSuccess ? "success" : "failure") . "\">" .
                $this->_responseMessage . "<div class=\"miniText\">" . $this->_responseSubtext . "</div></div>\n";
		}
        
        echo "<form method=\"POST\" action=\"" . WEB_ROOT . "/contact/\" class=\"contact\">\n";
        echo "  <label for=\"name\">Your Name:</label>";
        echo "  <input type=\"text\" name=\"name\" id=\"name\" value=\"" . htmlentities($this->_cachedName) . "\" />\n";
        echo "  <label for=\"email\">Your E-mail:</label>";
        echo "  <input type=\"email\" name=\"email\" id=\"email\" value=\"" . htmlentities($this->_cachedEmail) .
            "\" pattern=\"[^ @]*@[^ @]*\" />\n";
        echo "  <label for=\"message\">Message:</label>";
        echo "  <textarea name=\"message\" id=\"message\" rows=\"7\">" . htmlentities($this->_cachedMessage) .
            "</textarea>";
        echo "  <input type=\"submit\" value=\"Send message\" />\n";
        echo "</form>\n";
        
        $randomQuote = $this->_emailQuotes[array_rand($this->_emailQuotes)];
        echo "<quote data-speaker=\"" . $randomQuote[1] . "\">" . $randomQuote[0] . "</quote>";
    }
}
?>