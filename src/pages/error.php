<?php
class Page
{
    function preRender($database, $arguments)
    {
    }
    function outputColumn($database, $arguments)
	{
        echo "<div><strong>Why am I here?</strong> Well, you probably made a mistake. You typed the page in incorrectly, " . 
            "or you thought a file existed here when clearly it doesn't, or you thought you could <i>break my code " .
            "somehow</i>. In any event, it evidently hasn't turned out the way you were expecting.</div>\n";
            
        echo "<div><strong>But I didn't make a mistake!</strong> Funny, that makes two of us.</div>\n";
	}
    function output($database, $arguments)
    {
        echo "<h1>Error: Something yada yada</h1>\n";
        echo "<p>Well, welcome to the end of the internet. You can't go any further. Maybe you took a wrong turn and tried " .
            "going somewhere that doesn't exist. Or perhaps you thought you could sneak past the security guards and make " .
            "a break for somewhere you aren't allowed. Perhaps you broke your browser and my server just feels bad for you. " .
            "I mean, I know why you can't go any further. But that information doesn't really matter to you. It's not " .
            "as if you'd be able to do anything <i>with</i> it anyways &mdash; the fact of the matter is: <i>this is the " .
            "end of the road</i>. So, why don't we just turn around and <a href=\"javascript:history.go(-1);\">go back the " .
            "way we came in</a>?</p>";
    }
}
?>