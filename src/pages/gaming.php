<?php
class Page
{
    function preRender($database, $arguments)
    {
        require_once (DOCUMENT_ROOT . "/lib/gaming.inc.php");
    }
    function outputColumn($database, $arguments)
	{
	}
    function output($database, $arguments)
    {
        $games = $database->query("SELECT handle, name, platform, completion, monthFirstBegun, yearFirstBegun, monthFirstCompleted, yearFirstCompleted, " .
            "personalRating FROM games");
        echo "<table class=\"games\">\n";
        while ($game = $games->fetchArray())
        {
            echo "<tr class=\"game\">\n";
            echo "  <td rowspan=\"3\" class=\"icon\"><img src=\"/images/games/" . $game["handle"] . ".png\" /></td>";
            echo "  <td class=\"title\">" . $game["name"] . "</td>\n";
            echo "</tr>\n";
            echo "<tr>\n";
            echo "  <td class=\"info\"><span class=\"label\">Platform:</span> " . getConsoleName($game["platform"]) . " | ";
            
            if ($game["yearFirstBegun"] != null || $game["yearFirstCompleted"] != null)
            {
                echo "<span class=\"label\">Initial playthrough:</span> ";
                if ($game["yearFirstBegun"] != null)
                {
                    if ($game["monthFirstBegun"] != null)
                    {
                        echo ucfirst($game["monthFirstBegun"]) . " ";
                    }
                    echo $game["yearFirstBegun"];
                }
                else
                {
                    echo "<span class=\"unknown\">Unknown</span>";
                }
                
                if ($game["yearFirstCompleted"] != $game["yearFirstBegun"] || $game["monthFirstCompleted"] != $game["monthFirstBegun"])
                {
                    echo " &ndash; ";
                    
                    if ($game["yearFirstCompleted"] != null)
                    {
                        if ($game["monthFirstCompleted"] != null)
                        {
                            echo ucfirst($game["monthFirstCompleted"]) . " ";
                        }
                        echo $game["yearFirstCompleted"];
                    }
                    else
                    {
                        echo "<span class=\"unknown\">Unknown</span>";
                    }
                }
                
                echo " | ";
            }
            
            echo "<span class=\"label\">Play Status:</span> ";
            switch ($game["completion"])
            {
                case "evenReplayed":
                {
                    echo "<span class=\"green\">I've even replayed it!</span>";
                    break;
                }
                case "abandoned":
                {
                    echo "<span class=\"red\">I couldn't keep playing this.</span>";
                    break;
                }
                default:
                {
                    echo ucfirst($game["completion"]);
                    break;
                }
            }
            echo "</td>\n";
            echo "</tr>\n";
            
            echo "<tr>\n";
            echo "  <td>...</td>\n";
            echo "</tr>\n";
        }
        echo "</table>";
    }
}
?>