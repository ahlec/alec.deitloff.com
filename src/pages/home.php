<?php
class Page
{
    private $_postsPerPage = 10.0;
    private $_currentPage = 1;
    private $_totalPages;
    function preRender($database, $arguments)
    {
        $this->_totalPages = ceil($database->querySingle("SELECT count(*) FROM socialMediaPosts") /
            $this->_postsPerPage);
        if (isset($arguments[0]) && strpos($arguments[0], "page-") === 0)
        {
            $pageNumber = substr($arguments[0], 5);
            if (ctype_digit($pageNumber))
            {
                if ($pageNumber <= 1)
                {
                    $this->_currentPage = 1;
                }
                else if ($pageNumber > $this->_totalPages)
                {
                    $this->_currentPage = $this->_totalPages;
                }
                else
                {
                    $this->_currentPage = $pageNumber;
                }
            }
        }
    }
    
    function getKeyDownFunction()
    {
        return "keydownEventHandler(event, " . $this->_currentPage . ", " .
            ($this->_currentPage > 1 ? "true" : "false") . ", " . ($this->_currentPage <
            $this->_totalPages ? "true" : "false") . ");";
    }
    
    function outputColumn($database, $arguments)
	{
        echo "<div><strong>And what do we have here?</strong> I have many different accounts on many different " .
            "websites across the internet. On each site or service, I do something &mdash; post a status, achieve " .
            "a reward &mdash; that tells another tiny piece of who I am. Or sometimes, it's just a funny joke " .
            "that I posted. I am, after all, quite funny. So here is a central hub of my activities on the internets. " .
            "My social media accounts are tied into this website, so that anytime I do something somewhere, it shows " .
            "up here. So basically, if you want to see me do things, this would be the place to check ;)</div>";
            
        echo "<div><strong>Are you all over the internet?</strong> Pretty much.</div>\n";
	}
    function output($database, $arguments)
    {
        echo "<h1><a href=\"" . WEB_ROOT . "/\">Latest updates</a></h1>\n";
        if ($this->_currentPage > 1)
        {
            echo "<a href=\"" . WEB_ROOT . "/home/page-" . ($this->_currentPage - 1) .
                "/\" class=\"socialMediaLink prev\">Page " . ($this->_currentPage - 1) . "</a>";
        }
        if ($this->_currentPage < $this->_totalPages)
        {
            echo "<a href=\"" . WEB_ROOT . "/home/page-" . ($this->_currentPage + 1) .
                "/\" class=\"socialMediaLink next\">Page " . ($this->_currentPage + 1) . "</a>";
        }
        if ($this->_totalPages > 1)
        {
            echo "<div class=\"socialMediaPages\">Page " . $this->_currentPage . " of " . $this->_totalPages . "</div>";
        }
    
        $posts = $database->query("SELECT `dateTime`, `fullText`, localFile AS \"avatar\", fullName AS \"site\", " .
            "siteURL, accountName, profileURL, siteHandle, isHTML, postVerb, IFNULL(alternateAccountName, accountName) AS \"sidebarName\", " .
            "IFNULL(alternateAccountURL, profileURL) AS \"profileURL\", prependProfileURL, overlay AS \"avatarOverlay\" FROM socialMediaPosts JOIN " .
            "socialMediaAvatars ON socialMediaPosts.avatar = socialMediaAvatars.avatarID AND socialMediaPosts.site = " .
            "socialMediaAvatars.site JOIN socialMedia ON socialMediaPosts.site = socialMedia.siteHandle ORDER BY " .
            "`dateTime` DESC LIMIT " . $this->_postsPerPage . " OFFSET " . ($this->_currentPage - 1) *
            $this->_postsPerPage);
        while ($post = $posts->fetchArray())
        {
            echo "<div class=\"socialMediaPost " . $post["siteHandle"] . "\">\n";
            echo "  <div class=\"side\"><a href=\"" . $post["profileURL"] . "\" target=\"_blank\" title=\"" . $post["sidebarName"] .
                "\" alt=\"" . $post["sidebarName"] . "\" class=\"avatar\"" . ($post["avatarOverlay"] != null ? " data-overlay=\"" .
                $post["avatarOverlay"] . "\"" : "") . "><img src=\"" .
                WEB_ROOT . "/images/avatars/" . $post["avatar"] . "\" border=\"0\" /></a></div>\n";
            echo "<span>";
            if ($post["isHTML"] == "1")
            {
                echo $post["fullText"];
            }
            else
            {
                echo $post["fullText"];
   //             echo htmlentities(utf8_decode($post["fullText"]));
            }
            echo "</span>";
            echo "  <div class=\"meta\"><a href=\"" . $post["profileURL"] . "\" target=\"_blank\"><b>" .
                $post["accountName"] . "</b> on <b>" . $post["site"] . "</b></a> | <b>" . $post["postVerb"] . "</b> " .
                date(DATETIME_FORMAT, strtotime($post["dateTime"])) . "";
            if ($post["siteURL"] != null)
            {
                echo " | <a href=\"" . ($post["prependProfileURL"] ? $post["profileURL"] : "") .
					$post["siteURL"] . "\" target=\"_blank\">See this <b>on " . $post["site"] . "</b></a>";
            }
            echo "</div>\n";
            echo "</div>\n";
        }
        
        if ($this->_currentPage > 1)
        {
            echo "<a href=\"" . WEB_ROOT . "/home/page-" . ($this->_currentPage - 1) .
                "/\" class=\"socialMediaLink prev\">Page " . ($this->_currentPage - 1) . "</a>";
        }
        if ($this->_currentPage < $this->_totalPages)
        {
            echo "<a href=\"" . WEB_ROOT . "/home/page-" . ($this->_currentPage + 1) .
                "/\" class=\"socialMediaLink next\">Page " . ($this->_currentPage + 1) . "</a>";
        }
        if ($this->_totalPages > 1)
        {
            echo "<div class=\"socialMediaPages\">Page " . $this->_currentPage . " of " . $this->_totalPages . "</div>";
        }
    }
}
?>