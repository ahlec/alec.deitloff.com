<?php
class Page
{
    function preRender($database, $arguments)
    {
    }
    function outputColumn($database, $arguments)
	{
        echo "<div><strong>What languages do I know?</strong>I'm glad you were forced to ask! I know the following " .
            "languages (or pseudo-languages), in alphabetical order: <ul class=\"languages\">";
        echo "<li>Basic</li>\n";
        echo "<li>C</li>\n";
        echo "<li>C#<ul><li>XNA</li></ul></li>\n";
        echo "<li>C++</li>\n";
        echo "<li>CSS</li>\n";
        echo "<li>HTML</li>\n";
        echo "<li>Java</li>\n";
        echo "<li>JavaScript<ul><li>AJAX</li></ul></li>\n";
        echo "<li>Lua</li>\n";
        echo "<li>MediaWiki</li>\n";
        echo "<li>MIPS</li>\n";
        echo "<li>PHP</li>\n";
        echo "<li>Python</li>\n";
        echo "<li>RelaxNG</li>\n";
        echo "<li>Schematron</li>\n";
        echo "<li>SQL<ul><li>MySQL</li><li>SQLite</li></ul></li>\n";
        echo "<li>XML</li>\n";
        echo "<li>XPath</li>\n";
        echo "<li>XQuery</li>\n";
        echo "<li>XSLT</li>\n";
        echo "</ul></div>\n";
        
        echo "<div><strong>Do you have a favourite language?</strong> That's a tricky question. First off, there are " .
            "the two <i>types</i> of programming &mdash; web and desktop. For web programming, my favourite language " .
            "would undoubtedly be PHP. But for desktop programming, I seem to be torn between C# and C++. On the one " .
            "hand, I absolutely love the syntax of C#, and how it just... seems to get everything <i>right</i>. But " .
            "I cannot deny the sheer strength, power, portability, and universality of C++.</div>\n";
        
        echo "<div><strong>A <i>least</i> favourite, then?</strong> Java.</div>\n";
	}
    function output($database, $arguments)
    {
        echo "<h1>Programming</h1>\n";
        $programs = $database->query("SELECT programID, handle, name, hostedLink, goal, status, statusDate, " .
            "description FROM programs ORDER BY IF(statusDate IS NULL, 0, 1) ASC, statusDate DESC");
        while ($program = $programs->fetchArray())
        {
            $languages = $database->query("SELECT language FROM programsLanguages WHERE program='" . $database->escapeString($program["programID"]) . "'");
            echo "<div class=\"program\">\n";
            echo "<div class=\"banner\" style=\"background-image:url('/images/programs/" .
                $program["handle"] . ".png');\"><span>" . $program["name"] . "</span></div>\n";
            while ($language = $languages->fetchArray())
            {
                echo "<div class=\"language ";
                switch ($language["language"])
                {
                    case "c#":
                    {
                        echo "csharp";
                        break;
                    }
                    case "c++":
                    {
                        echo "cpp";
                        break;
                    }
                    default:
                    {
                        echo $language["language"];
                        break;
                    }
                }
                echo "\"></div>\n";
            }
            echo "<table class=\"information\">\n";
            echo "  <tr><th>Name:</th><th>Homepage:</th></tr>\n";
            echo "  <tr><td>" . $program["name"] . "</td><td>";
            if ($program["hostedLink"] != null)
            {
                echo "<a href=\"" . $program["hostedLink"] . "\" target=\"_blank\">" . $program["hostedLink"] .
                    "</a>";
            }
            else
            {
                echo "<i>Not hosted at this time</i>";
            }
            echo "</td></tr>\n";
            echo "  <tr><th>Status:</th><th>Project goal/purpose:</th></tr>\n";
            echo "  <tr><td>";
            switch ($program["status"])
            {
                case "active":
                {
                    echo "Active development";
                    break;
                }
                default:
                {
                    echo ucfirst($program["status"]);
                    if ($program["status"] == "hiatus")
                    {
                        echo " (since ";
                    }
                    else
                    {
                        echo " (";
                    }
                    echo date("F Y", strtotime($program["statusDate"]));
                    echo ")";
                    break;
                }
            }
            echo "</td><td>";
            switch ($program["goal"])
            {
                case "poc":
                {
                    echo "Proof of concept";
                    break;
                }
                default:
                {
                    echo ucfirst($program["goal"]);
                    break;
                }
            }
            echo "</td></tr>\n";
            echo "</table>";
            echo "<div class=\"description\">" . $program["description"] . "</div>\n";
            echo "</div>\n";
        }
    }
}
?>