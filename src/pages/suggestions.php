<?php
class Page
{
    private $_attemptedSuggest = false;
    private $_suggestSuccess = false;
    private $_sortColumn = "IF(status='inProgress', 1, IF(status='pending', 2, IF(status='completed', 3, 4))) ASC, votes";
    private $_sortDirection = "DESC";
  
    function preRender($database, $arguments)
    {
	if (isset($_POST["suggestion"]) && mb_strlen($_POST["suggestion"]) > 0)
	{
		$this->_attemptedSuggest = true;
		$this->_suggestSuccess = $database->exec("INSERT INTO suggestionsQueue(input, submitted) VALUES('" .
			$database->escapeString($_POST["suggestion"]) . "','" .
			date("Y-m-d H:i:s") . "')");
	}

	if (isset($_SESSION[SUGGESTION_DIRECTION]))
	{
		if ($_SESSION[SUGGESTION_DIRECTION] == "ASC")
		{
			$this->_sortDirection = "ASC";
		}
	}
	if (isset($_SESSION[SUGGESTION_COLUMN]))
	{
		if ($_SESSION[SUGGESTION_COLUMN] == "name")
		{
			$this->_sortColumn = "name";
		}
		else if ($_SESSION[SUGGESTION_COLUMN] == "suggestionType")
		{
			$this->_sortColumn = "CONCAT(suggestionType)";
		}
	}
    }
    function outputColumn($database, $arguments)
    {
        echo "<div><strong>What is this?</strong> A while back, I realized that people were suggesting that I read this book or watch " .
		"some movie, because \"[they're all] great!\" Which I'm sure they are, but I wasn't recording them. Someone would say " .
		"something, and I'd just let it slip away. So I built this page to allow my friends (or strangers) to suggest that I watch " .
		"such-and-such show or listen to \"this amazing band.\" And I thought: \"while I'm at it, why don't I make it " .
		"self-sustaining? Why don't I make the program figure out <i>exactly</i> what that person is suggesting to me, and then " .
		"categorize it?\" So I did.</div>";
        echo "<div><strong>How does it work?</strong> What happens when you submit is that your input " .
		"will be sent to Google to locate the properly spelled, properly formatted name for whatever you submitted, and then " .
		"references Wikipedia to determine what type of... thing you've submitted. All of this takes time, and so this " .
		"<i>won't happen when you press Suggest</i>. This process happens usually once per hour of its own accord. So check " .
		"back here often!</div>";
        echo "<div><strong>What happens for duplicate submissions?</strong> If two people (or one very " .
		"adamant person) submit the same suggestion, it will only be added to the table below once. However, it will be marked " .
		"with then having two votes. If someone submits the suggestion again, it will have three votes. Items with higher vote " .
		"counts appear higher on the table, and I'll notice that people really want me to verb that suggestion.</div>";
    }
    function output($database, $arguments)
    {
		echo "<h1>Suggest something</h1>\n";
		if ($this->_attemptedSuggest)
		{
			echo "<div class=\"status " . ($this->_suggestSuccess ? "success" : "failure") . "\">";
			if ($this->_suggestSuccess)
			{
				echo "Suggestion received!<div class=\"miniText\">Thank you for your submission!</div>\n";
			}
			else
			{
				echo "Problem encountered!<div class=\"miniText\">Something exploded nearby. I've already sent out the worker drones.</div>\n";
			}
			echo "</div>\n";
		}

		echo "  <form method=\"post\" action=\"" . WEB_ROOT . "/suggestions/\">\n";
		echo "    <input type=\"text\" name=\"suggestion\" class=\"suggestion_box\" />\n";
		echo "    <input type=\"submit\" value=\"Suggest!\" class=\"suggestion_submit\" />\n";
		echo "    <div class=\"suggestionExamples\">Examples: 'pokemon red', 'homstuck', 'adeventure time', 'ikimono gakari'</div>\n";
		echo "  </form>\n";
		
		echo "<h1>Suggestions</h1>";
		echo "  <table class=\"suggestions\">\n";
		echo "    <tr class=\"header\">\n";
		echo "      <td id=\"type\"><div class=\"iconWrapper\">Type <img src=\"" . WEB_ROOT . "/style/images/" .
			($this->_sortDirection == "ASC" && $this->_sortColumn == "CONCAT(suggestionType)" ? "asc" : "desc") .
			".png\" class=\"sortIcon" .	($this->_sortColumn == "CONCAT(suggestionType)" ? " current" : "") .
			"\" title=\"Reorder\" onclick=\"suggestions_sort('suggestionType');\" /></div></td>\n";
		echo "      <td id=\"name\"><div class=\"iconWrapper\">Name <img src=\"" . WEB_ROOT . "/style/images/" .
			($this->_sortDirection == "ASC" && $this->_sortColumn == "name" ? "asc" : "desc") .
			".png\" class=\"sortIcon" . ($this->_sortColumn == "name" ? " current" : "") . "\" title=\"Reorder\" " .
			"onclick=\"suggestions_sort('name');\" /></div></td>\n";
		echo "      <td id=\"votes\"><div class=\"iconWrapper\">Votes for <img src=\"" . WEB_ROOT .
			"/style/images/" . ($this->_sortDirection == "ASC" && $this->_sortColumn ==
			"IF(status='inProgress', 1, IF(status='pending', 2, IF(status='completed', 3, 4))) ASC, votes" ? "asc" : "desc") .
			".png\" class=\"sortIcon" . ($this->_sortColumn ==
			"IF(status='inProgress', 1, IF(status='pending', 2, IF(status='completed', 3, 4))) ASC, votes" ? " current" : "") .
			"\" title=\"Reorder\" onclick=\"suggestions_sort('votes');\" /></div></td>\n";
		echo "    </tr>\n";
		
		$suggestions = $database->query("SELECT suggestionID, name, suggestionType, votes, status FROM " .
			"suggestions ORDER BY " . $this->_sortColumn . " " . $this->_sortDirection);
		while ($suggestion = $suggestions->fetchArray())
		{
			echo "    <tr class=\"" . $suggestion["status"] . "\">\n";
			echo "      <td>";
			switch ($suggestion["suggestionType"])
			{
				case "movie":
				{
					echo "Movie";
					break;
				}
				case "television show":
				{
					echo "TV Show";
					break;
				}
				case "anime":
				{
					echo "Anime";
					break;
				}
				case "web series":
				{
					echo "Web series";
					break;
				}
				case "manga":
				{
					echo "Manga";
					break;
				}
				case "comic":
				{
					echo "Comic";
					break;
				}
				case "webcomic":
				{
					echo "Webcomic";
					break;
				}
				case "youtube channel":
				{
					echo "YouTube channel";
					break;
				}
				case "computer game":
				{
					echo "PC Game";
					break;
				}
				case "ds game":
				{
					echo "DS Game";
					break;
				}
				case "3ds game":
				{
					echo "3DS Game";
					break;
				}
				case "wii game":
				{
					echo "Wii Game";
					break;
				}
				case "xbox game":
				{
					echo "Xbox Game";
					break;
				}
				case "xbox360 game":
				{
					echo "Xbox360 Game";
					break;
				}
				case "gba game":
				{
					echo "GBA Game";
					break;
				}
                case "gbc game":
                {
                    echo "Gameboy Color Game";
                    break;
                }
				case "gamecube game":
				{
					echo "GameCube Game";
					break;
				}
				case "mmorpg":
				{
					echo "MMORPG";
					break;
				}
				case "ps2 game":
				{
					echo "PS2 Game";
					break;
				}
				case "ps1 game":
				{
					echo "PS1 Game";
					break;
				}
				case "ps3 game":
				{
					echo "PS3 Game";
					break;
				}
				case "psp game":
				{
					echo "PSP Game";
					break;
				}
				case "band":
				{
					echo "Band";
					break;
				}
				case "novel":
				{
					echo "Novel";
					break;
				}
                case "mythology":
                {
                    echo "Mythology";
                    break;
                }
			}
			echo "</td>\n";
			echo "      <td><a href=\"http://en.wikipedia.org/wiki/" . $suggestion["name"] . "\" target=\"_blank\">" .
				$suggestion["name"] . "</a></td>\n";
			echo "      <td><div class=\"suggestionsVoteWrapper\">";
			if ($suggestion["status"] == "pending")
			{
				echo "<img src=\"" . WEB_ROOT . "/style/images/up_vote.png\" onclick=\"suggestions_vote('" .
					$suggestion["suggestionID"] . "');\" class=\"upVote\" title=\"Vote for this suggestion!\" /> " .
					"<span id=\"votes-" . $suggestion["suggestionID"] . "\">" . $suggestion["votes"] . "</span>";
			}
			else if ($suggestion["status"] == "completed")
			{
				echo "<img src=\"" . WEB_ROOT .
				"/style/images/complete.png\" title=\"Already done!\" class=\"statusIcon\" /> Already done!";
			}
			else if ($suggestion["status"] == "inProgress")
			{
				echo "<img src=\"" . WEB_ROOT .
					"/style/images/in_progress.png\" title=\"I'm currently working on it...\" " .
					"class=\"statusIcon\" /> Working on it...";
			}
			else if ($suggestion["status"] == "declined")
			{
				echo "<img src=\"" . WEB_ROOT .
					"/style/images/declined.png\" title=\"Humbly declined from this suggestion.\" " .
					"class=\"statusIcon\" /> Humbly declined :(";
			}
			echo "</div></td>\n";
			echo "    </tr>\n";
		}
		
		echo "  </table>\n";
    }
}
?>