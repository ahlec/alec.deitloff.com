<?php
class Page
{
    function preRender($database, $arguments)
    {
    }
    function outputColumn($database, $arguments)
	{
        echo "<div><strong>You teach?</strong> \"Teach\" is a strong word here. Being an undergraduate myself " .
            "right now, I'm not in the position to hold my own lectures, so in the literal sense, no, I don't " .
            "\"teach.\" However, I am an undergraduate teaching assistant for the computer science department " .
            "at the University of Pittsburgh, and in that capacity, I do my best to make sure that the students " .
            "I have understand the material, and if they don't, to provide the additional help they need to " .
            "fill it in. So I'm somewhere in the gray-area \"teach.\"</div>\n";
	}
    function output($database, $arguments)
    {
        echo "<h1>CS0008 (Spring 2013)</h1>\n";
        echo "<p>I was an undergraduate teaching assistant for a relatively new computer science course at the " .
            "University of Pittsburgh, <a href=\"https://www.cs.pitt.edu/undergrad/courses/cs0008.php\" target=\"_blank\"" .
            ">CS0008: Introduction to computer programming with Python</a>. Holding a once-a-week recitation and " .
            "office hours, I was responsible for helping non-programmers to adjust to their first programming " .
            "language: Python. I wrote a number of weekly exercises to further test the students on their knowledge " .
            "of the language; these can be found <a href=\"http://www.deitloff.com/cs0008/\" target=\"_blank\">here</a>.</p>";
    }
}
?>