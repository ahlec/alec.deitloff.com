<?php
require_once ("./lib/config.inc.php");
$database = database();

$path_pieces = explode("/", (isset($_GET["path"]) ? trim($_GET["path"], "/\t\n \r") : ""));
if (count($path_pieces) == 0 || mb_strlen($path_pieces[0]) == 0 || ctype_space($path_pieces[0]))
{
    $path_pieces = array("home");
}
$saved_path_pieces = $path_pieces;

$basePage = null;
$initialCheck = $database->querySingle("SELECT handle, isSprawlingParent, foreignTable, foreignKey, traversalLeft, " .
    "traversalRight FROM pages WHERE (parentHandle IS NULL OR parentHandle='home') AND handle='" .
    $database->escapeString(mb_strtolower($path_pieces[0])) . "' LIMIT 1", true);
$foreignKeys = array();
if ($initialCheck === false)
{
    /*if ($path_pieces[0] != "." && $path_pieces[0] != ".." && is_dir(BASE_DOCUMENT_ROOT . "/" . $path_pieces[0]) &&
        realpath(BASE_DOCUMENT_ROOT . "/" . $path_pieces[0]) != DOCUMENT_ROOT)
    {
        header("Location: " . BASE_WEB_ROOT . "/" . implode("/", $path_pieces));
        exit();
    }   
    array_unshift($path_pieces, "404");
    $basePage = "error";*/
    exit ("initial check failed");
}
else
{
    $traversalLeft = $initialCheck["traversalLeft"];
    $traversalRight = $initialCheck["traversalRight"];
    $basePage = $initialCheck["handle"];
    $isSprawlingParent = $initialCheck["isSprawlingParent"];
    $foreignTable = $initialCheck["foreignTable"];
    $foreignKey = $initialCheck["foreignKey"];
    array_shift($path_pieces);
    $hasPreparedStatement = false;
    while (count($path_pieces) > 0)
    {
        if (!$isSprawlingParent)
        {
            $deeperCheck = $database->querySingle("SELECT handle, isSprawlingParent, foreignTable, foreignKey, " .
                "traversalLeft, traversalRight FROM pages WHERE parentHandle='" . $database->escapeString($basePage) .
                "' AND parentHandle IS NOT NULL AND parentHandle<>'home' AND traversalLeft > '" .
                $database->escapeString($traversalLeft) . "' AND traversalRight < '" .
                $database->escapeString($traversalRight) . "' AND handle='" .
                $database->escapeString(mb_strtolower($path_pieces[0])) . "' LIMIT 1", true);
            if ($deeperCheck === false)
            {
                break;
            }
        }
        else
        {
            /*if (!$hasPreparedStatement)
            {
                $database->exec("SET @pageForeignQuery = 'SELECT ? AS \"handle\" FROM ?'; PREPARE pageForeign FROM @pageForeignQuery;");
                $hasPreparedStatement = true;
            }*/
            $foreignEntry = $database->querySingle("SELECT " . $foreignKey . " AS \"handle\" FROM " . $foreignTable . " WHERE " .
                $foreignKey . "='" . $database->escapeString(mb_strtolower($path_pieces[0])) . "' LIMIT 1", true);
            if ($foreignEntry === false)
            {
                break;
            }
            $deeperCheck = $database->querySingle("SELECT handle, isSprawlingParent, foreignTable, foreignKey, " .
                "traversalLeft, traversalRight FROM pages WHERE parentHandle='" . $database->escapeString($basePage) .
                "' AND parentHandle IS NOT NULL AND parentHandle<>'home' AND traversalLeft > '" .
                $database->escapeString($traversalLeft) . "' AND traversalRight < '" .
                $database->escapeString($traversalRight) . "' LIMIT 1", true);
            if ($deeperCheck === false)
            {
                exit("[NO ACCEPTING PAGE FOR FOREIGN ENTRY]");
            }
            $foreignKeys[$deeperCheck["handle"]] = $foreignEntry["handle"];
        }
        $traversalLeft = $deeperCheck["traversalLeft"];
        $traversalRight = $deeperCheck["traversalRight"];
        $basePage = $deeperCheck["handle"];
        $isSprawlingParent = $initialCheck["isSprawlingParent"];
        $foreignTable = $initialCheck["foreignTable"];
        $foreignKey = $initialCheck["foreignKey"];
        array_shift($path_pieces);
    }
    
    if ($hasPreparedStatement)
    {
        $database->exec("DEALLOCATE PREPARE pageForeign;");
    }
}

/*$base_page = $database->querySingle("SELECT fileName FROM pages WHERE handle='" . $database->escapeString($path_pieces[0]) . "' LIMIT 1", true);
if ($base_page === false)
{
    array_unshift($path_pieces, "error", "404");
}*/

$pageInfo = $database->querySingle("SELECT fileName, title, stylesheetFile, javascriptFile, traversalLeft, " .
	"traversalRight FROM pages WHERE handle='" . $database->escapeString($basePage) . "' LIMIT 1", true);
if (dirname(DOCUMENT_ROOT . "/pages/" . $pageInfo["fileName"]) != DOCUMENT_ROOT . "/pages")
{
    array_unshift($path_pieces, "500");
    $pageInfo = $database->querySingle("SELECT fileName, title, stylesheetFile, javascriptFile, traversalLeft, " .
        "traversalRight FROM pages WHERE handle='error' LIMIT 1", true);
}
//$pageName = array_shift($path_pieces);

/* load page */
//require_once (DOCUMENT_ROOT . "/pages/" . $pageInfo["fileName"]);
//$page = new Page();

echo "<pre>";
print_r($pageInfo);
print_r($path_pieces);
print_r($foreignKeys);
echo "</pre>";
?>