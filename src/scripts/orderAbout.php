<?php
require_once ("../lib/config.inc.php");
$database = database();

if ($database == null)
{
    require_once (DOCUMENT_ROOT . "/lib/index-majorError.php");
    exit();
}

if (!isset($_GET["aboutID"]))
{
    exit("NEED aboutID");
}

/* get answers */
$answer = $database->querySingle("SELECT aboutID, width, question, answer FROM about WHERE aboutID='" .
    $database->escapeString($_GET["aboutID"]) . "' LIMIT 1", true);

if ($answer == false)
{
    exit("INVALID");
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Jacob Deitloff</title>
    <meta charset="UTF-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/base.css" />
    <link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/page-about.css" />
  </head>
  <body onload="document.title = document.getElementById('sandbox').offsetHeight;">
	<div class="container">
		<div class="body" id="body">
			<div class="column"></div>
            <div class="aboutBox one">one</div>
            <div class="aboutBox one">one</div>
            <div class="aboutBox one">one<br>one</div>
            <div class="aboutBox two">two</div>
            <div style="clear:both;"></div>
            <div class="aboutBox <?php echo $answer["width"]; ?>" id="sandbox">
                <div class="question"><?php echo $answer["question"]; ?></div>
                <div class="answer"><?php echo $answer["answer"]; ?></div>
            </div>
        </div>
	</div>
  </body>
</html>