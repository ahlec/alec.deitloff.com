<?php
if (!isset($_GET["id"]) || !ctype_digit($_GET["id"]))
{
	exit("Invalid id parameter, or no id parameter.");
}

require_once ("../lib/config.inc.php");
$database = database();

$askId = $database->escapeString($_GET["id"]);
if ($database->querySingle("SELECT count(*) FROM ask WHERE askId='" . $askId . "' AND status='pending'") <= 0)
{
	exit ("This question no longer seems to be waiting to be answered.");
}

if ($database->querySingle("SELECT votes FROM ask WHERE askId='" . $askId . "' LIMIT 1") >= MAX_ASK_VOTES)
{
    exit ("This question has already received enough votes! I'll make sure to address it :)");
}

if ($database->exec("UPDATE ask SET votes = votes + 1 WHERE askId='" . $askId . "'"))
{
	echo "success-";
	exit ($database->querySingle("SELECT votes FROM ask WHERE askId='" . $askId . "' LIMIT 1"));
}

exit ("Could not tally vote.");
?>