<?php
if (!isset($_GET["column"]))
{
	exit("There was no specification about which column we should sort by!");
}

require_once ("../lib/config.inc.php");

if (isset($_SESSION[ASK_COLUMN]) && $_SESSION[ASK_COLUMN] == $_GET["column"] ||
	!isset($_SESSION[ASK_COLUMN]) && $_GET["column"] == "status")
{
	if (isset($_SESSION[ASK_DIRECTION]) && $_SESSION[ASK_DIRECTION] == "ASC")
	{
		$_SESSION[ASK_DIRECTION] = "DESC";
	}
	else
	{
		$_SESSION[ASK_DIRECTION] = "ASC";
	}
	exit ("success");
}

if ($_GET["column"] == "status" || $_GET["column"] == "question")
{
	$_SESSION[ASK_COLUMN] = $_GET["column"];
	$_SESSION[ASK_DIRECTION] = ($_GET["column"] == "status" ? "DESC" : "ASC");
	exit ("success");
}

exit ("It seems you tried sorting in some weird way. Why not try and stick with the program, bub?");
?>
