<?php
if (!isset($_GET["column"]))
{
	exit("No column parameter");
}

require_once ("../lib/config.inc.php");

if (isset($_SESSION[SUGGESTION_COLUMN]) && $_SESSION[SUGGESTION_COLUMN] == $_GET["column"] ||
	!isset($_SESSION[SUGGESTION_COLUMN]) && $_GET["column"] == "votes")
{
	if (isset($_SESSION[SUGGESTION_DIRECTION]) && $_SESSION[SUGGESTION_DIRECTION] == "ASC")
	{
		$_SESSION[SUGGESTION_DIRECTION] = "DESC";
	}
	else
	{
		$_SESSION[SUGGESTION_DIRECTION] = "ASC";
	}
	exit ("success");
}

if ($_GET["column"] == "votes" || $_GET["column"] == "suggestionType" || $_GET["column"] == "name")
{
	$_SESSION[SUGGESTION_COLUMN] = $_GET["column"];
	$_SESSION[SUGGESTION_DIRECTION] = ($_GET["column"] == "votes" ? "DESC" : "ASC");
	exit ("success");
}

exit ("Invalid input");
?>
