<?php
if (!isset($_GET["id"]) || !ctype_digit($_GET["id"]))
{
	exit("Invalid id parameter, or no id parameter.");
}

require_once ("../lib/config.inc.php");
$database = database();

$suggestion_id = $database->escapeString($_GET["id"]);
if ($database->querySingle("SELECT count(*) FROM suggestions WHERE suggestionID='" . $suggestion_id . "' AND status='pending'") <= 0)
{
	exit ("No active suggestion with the provided id exists.");
}

if ($database->exec("UPDATE suggestions SET votes = votes + 1 WHERE suggestionID='" . $suggestion_id . "'"))
{
	echo "success-";
	exit ($database->querySingle("SELECT votes FROM suggestions WHERE suggestionID='" . $suggestion_id . "' LIMIT 1"));
}

exit ("Could not tally vote.");
?>