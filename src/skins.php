<?php
require_once ("./lib/config.inc.php");
$database = database();

if ($database == null)
{
    require_once (DOCUMENT_ROOT . "/lib/index-majorError.php");
    exit();
}

/* determine page */
$path_pieces = explode("/", (isset($_GET["path"]) ? trim($_GET["path"], "/\t\n \r") : ""));
if (count($path_pieces) == 0 || mb_strlen($path_pieces[0]) == 0 || ctype_space($path_pieces[0]))
{
    $path_pieces = array("home");
}
/*else
{
    $path_pieces[0] = mb_strtolower($path_pieces[0]);
}*/

$basePage = null;
$initialCheck = $database->querySingle("SELECT handle, traversalLeft, traversalRight FROM pages WHERE (parentHandle " .
    "IS NULL OR parentHandle='home') AND handle='" . $database->escapeString(mb_strtolower($path_pieces[0])) . "' LIMIT 1", true);
if ($initialCheck === false)
{
    if ($path_pieces[0] != "." && $path_pieces[0] != ".." && is_dir(BASE_DOCUMENT_ROOT . "/" . $path_pieces[0]) &&
        realpath(BASE_DOCUMENT_ROOT . "/" . $path_pieces[0]) != DOCUMENT_ROOT)
    {
        header("Location: " . BASE_WEB_ROOT . "/" . implode("/", $path_pieces));
        exit();
    }    
    array_unshift($path_pieces, "404");
    $basePage = "error";
}
else
{
    $traversalLeft = $initialCheck["traversalLeft"];
    $traversalRight = $initialCheck["traversalRight"];
    $basePage = $initialCheck["handle"];
    array_shift($path_pieces);
    while (count($path_pieces) > 0)
    {
        $deeperCheck = $database->querySingle("SELECT handle, traversalLeft, traversalRight FROM pages WHERE " .
            "parentHandle='" . $database->escapeString($basePage) .
            "' AND parentHandle IS NOT NULL AND parentHandle<>'home' AND traversalLeft > '" .
            $database->escapeString($traversalLeft) . "' AND traversalRight < '" .
            $database->escapeString($traversalRight) . "' AND handle='" .
            $database->escapeString(mb_strtolower($path_pieces[0])) . "' LIMIT 1", true);
        if ($deeperCheck === false)
        {
            break;
        }
        $traversalLeft = $deeperCheck["traversalLeft"];
        $traversalRight = $deeperCheck["traversalRight"];
        $basePage = $deeperCheck["handle"];
        array_shift($path_pieces);
    }
}

/*$base_page = $database->querySingle("SELECT fileName FROM pages WHERE handle='" . $database->escapeString($path_pieces[0]) . "' LIMIT 1", true);
if ($base_page === false)
{
    array_unshift($path_pieces, "error", "404");
}*/

$pageInfo = $database->querySingle("SELECT fileName, title, stylesheetFile, javascriptFile, traversalLeft, " .
	"traversalRight FROM pages WHERE handle='" . $database->escapeString($basePage) . "' LIMIT 1", true);
if (dirname(DOCUMENT_ROOT . "/pages/" . $pageInfo["fileName"]) != DOCUMENT_ROOT . "/pages")
{
    array_unshift($path_pieces, "500");
    $pageInfo = $database->querySingle("SELECT fileName, title, stylesheetFile, javascriptFile, traversalLeft, " .
        "traversalRight FROM pages WHERE handle='error' LIMIT 1", true);
}
//$pageName = array_shift($path_pieces);

/* load page */
require_once (DOCUMENT_ROOT . "/pages/" . $pageInfo["fileName"]);
$page = new Page();
$page->preRender($database, $path_pieces);

/* get breadcrumbs */
$breadcrumbsResults = $database->query("SELECT handle, title, breadcrumbLink FROM pages WHERE traversalLeft <= '" . $pageInfo["traversalLeft"] .
    "' AND traversalRight >= '" . $pageInfo["traversalRight"] . "' ORDER BY traversalDepth ASC");
$breadcrumbs = array();
while ($breadcrumb = $breadcrumbsResults->fetchArray())
{
    $breadcrumbs[$breadcrumb["handle"]] = array("title" => $breadcrumb["title"], "link" => ($breadcrumb["breadcrumbLink"] != "" ?
        $breadcrumb["breadcrumbLink"] : $breadcrumb["handle"]));
}

/* get console */
$consoleResults = $database->query("SELECT directory, command, `dateTime`, isSocialMedia, socialMediaPostID FROM console ORDER BY `dateTime` DESC LIMIT " . NUMBER_CONSOLE_COMMANDS);
$console = array();
while ($consoleResult = $consoleResults->fetchArray())
{
    $console[] = array("directory" => $consoleResult["directory"], "command" => $consoleResult["command"]);
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Jacob Deitloff</title>
	<base href="<?php echo WEB_ROOT; ?>" />
    <meta charset="UTF-8" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="The homepage of Jacob Deitloff, Computer Science and German language university student, and avid programmer." />
    <link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/base-skin.css" />
    <link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/<?php echo DEFAULT_SKIN; ?>/skin.css" />
    <link href='http://fonts.googleapis.com/css?family=Raleway:400' rel='stylesheet' type='text/css'>
	<script src="./style/jquery.js" type="text/javascript"></script>
	<script src="./style/console-2.js" type="text/javascript"></script>
	<script type="text/javascript">
		<?php
			foreach ($console as $consoleCommand)
			{
				echo "Console.addMessage('", preg_replace("/\n/", "", addslashes($consoleCommand["directory"])),
					"','", preg_replace("/\n/", "", addslashes($consoleCommand["command"])), "');\n";
			}
		?>
	</script>
    <script>
        var WEB_ROOT = '<?php echo WEB_ROOT; ?>';
    </script>
    <script src="./style/<?php echo DEFAULT_SKIN; ?>/header.js"></script>
    <?php
        if ($pageInfo["stylesheetFile"] != null)
        {
            echo "<link rel=\"stylesheet\" href=\"" . WEB_ROOT . "/style/" . $pageInfo["stylesheetFile"] .
                "\" />\n";
        }
        if ($pageInfo["javascriptFile"] != null)
        {
            echo "<script src=\"" . WEB_ROOT . "/style/" . $pageInfo["javascriptFile"] . "\"></script>";
        }
    ?>
  </head>
  <body onload="consoleStart(0);">
	<div class="header frame1" id="headerBackground">
		<div class="borderTop"></div>
		<div class="navbar"><?php
        echo "<a href=\"/\" class=\"home" . (count($breadcrumbs) == 1 && isset($breadcrumbs["home"]) ? " current" : "") . "\"></a>";
        foreach (array("about", "programming", "teaching", "suggestions", "contact") as $button)
        {
            echo "<a href=\"/" . $button . "/\" class=\"" . $button;
            if (count($breadcrumbs) > 1 && isset($breadcrumbs[$button]))
            {
                echo " current";
            }
            echo "\"></a>";
        }
        ?></div>
        <div class="navbarBottom"></div>
		<div class="borderBottom"></div>
        <div class="container" id="header">
            <a href="<?php echo WEB_ROOT; ?>/" class="logo" id="headerLogo"></a>
			<a href="<?php echo WEB_ROOT; ?>/" class="head" id="headerHead"></a>
            <div class="mario" id="mario" style="bottom:35px; right:380px;"></div>
            <div class="block" id="block1" style="bottom:102px; right:300px;"></div>
            <div class="block" id="block2" style="bottom:102px; right:316px;"></div>
            <div class="block" id="block3" style="bottom:102px; right:332px;"></div>
            <div class="goomba" style="right:225px;"></div>
            <div class="goomba" style="right:165px;"></div>
            <div class="bush" style="right:465px;"></div>
            <div class="bush" style="right:630px;"></div>
            <div class="pipe" style="right:520px;"></div>
            <div class="coin" style="right:120px; bottom:96px;"></div>
            <div class="coin" style="right:104px; bottom:112px;"></div>
            <div class="coin" style="right:88px; bottom:96px;"></div>
            <div class="coin" style="right:580px; bottom:80px;"></div>
            <div class="coin" style="right:580px; bottom:96px;"></div>
            <div class="coin" style="right:580px; bottom:112px;"></div>
        </div>
	</div>
	<div class="container">
		<div class="console">
			<div class="prompt"><span class="user">jacob&#64;deit<span class="botThrowoff">ff</span>.com</span> <span class="cwd" id="consoleCwd"><?php echo $console[0]["directory"]; ?></span></div>
			<div class="bash" id="consoleBash"><?php echo $console[0]["command"]; ?></div>
		</div>
		<div class="body">
			<div class="breadcrumbs"><div>
            <?php
                foreach ($breadcrumbs as $breadcrumb)
                {
                    echo "<span><a href=\"" . WEB_ROOT . "/" . $breadcrumb["link"] . "/\">" . $breadcrumb["title"] . "</a></span>\n";
                }
            ?>
            </div></div>
			<div class="column"><?php $page->outputColumn($database, $path_pieces); ?></div>
            <?php $page->output($database, $path_pieces); ?>
        </div>
	</div>
    <div class="footer">
        <div class="border"></div>
        <div class="text">
            &copy; 2013, Jacob Deitloff. All rights reserved, though I don't see why you'd want to claim anything as your own anyway. Mario, Super
            Mario Bros., and everything else regarding the theme of this layout are the sole property of Nintendo Inc., or anyone they designate it
            to; I do not own them (naturally). Font used for the navigation bar links is Pok&eacute;monDPPt, which is a rendition of the font from the GenIV
            Pok&eacute;mon games; ownership of that franchise belongs to GameFreak and Nintendo. Logo font is NSMBWii, a rendition of the font used in the
            Wii Mario game.
        </div>
    </div>
  </body>
</html>