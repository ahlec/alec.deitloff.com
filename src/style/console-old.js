var consoleFadeOutInterval;
var consoleFadeInInterval;
function consoleStart(currentIndex)
{
    setTimeout(function()
    {
        setTimeout("consoleFadeOut(" + currentIndex + ")", 50);
    }, 10000);
}
function consoleFadeOut(currentIndex)
{
    var cwd = document.getElementById("consoleCwd");
    var bash = document.getElementById("consoleBash");
    var newOpacity = (cwd.style.opacity != "" ? cwd.style.opacity : 1) * 0.5;
    if (newOpacity <= 0.05)
    {
        cwd.style.opacity = 0.05;
        bash.style.opacity = 0.05;
        cwd.style.filter = "alpha(opacity=0)";
        bash.style.filter = "alpha(opacity=0)";
        var newIndex = currentIndex + 1;
        if (newIndex == console.length)
        {
            newIndex = 0;
        }
        cwd.innerHTML = console[newIndex][0];
        bash.innerHTML = console[newIndex][1];
        setTimeout("consoleFadeIn(" + newIndex + ")", 50);
    }
    else
    {
        cwd.style.opacity = newOpacity;
        bash.style.opacity = newOpacity;
        cwd.style.filer = "alpha(opacity=" + (newOpacity * 100) + ")";
        bash.style.filter = "alpha(opacity=" + (newOpacity * 100) + ")";
        setTimeout("consoleFadeOut(" + currentIndex + ")", 50);
    }
}
function consoleFadeIn(currentIndex)
{
    var cwd = document.getElementById("consoleCwd");
    var bash = document.getElementById("consoleBash");
    var newOpacity = cwd.style.opacity * 2;
    if (newOpacity >= 1)
    {
        clearInterval(consoleFadeInInterval);
        cwd.style.opacity = 1.0;
        bash.style.opacity = 1.0;
        cwd.style.filter = "alpha(opacity=100)";
        bash.style.filter = "alpha(opacity=100)";
        consoleStart(currentIndex);
    }
    else
    {
        cwd.style.opacity = newOpacity;
        bash.style.opacity = newOpacity;
        cwd.style.filter = "alpha(opacity=" + (newOpacity * 100) + ")";
        bash.style.filter = "alpha(opacity=" + (newOpacity * 100) + ")";
        setTimeout("consoleFadeIn(" + currentIndex + ")", 50);
    }
}