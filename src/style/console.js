Console = {};
Console.DISPLAY_TIME = 10000;
Console.HEADER_ELEM_SELECTOR = "#consoleCwd";
Console.MESSAGE_ELEM_SELECTOR = "#consoleBash";

(function () {
	var messages = [],
		isConsoleActive = false,
		currentMessageIndex = -1,
		runConsole;
	
	function stopConsole() {
		isConsoleActive = false;
		console.log("Console has been stopped.");
	};
	
	runConsole = function() {
		if (!isConsoleActive) {
			stopConsole();
			return;
		}
		
		var headerElem = $(Console.HEADER_ELEM_SELECTOR),
			messageElem = $(Console.MESSAGE_ELEM_SELECTOR);
			
		if (!headerElem || !messageElem) {
			console.error("Could not locate one or more of the required DOM elements for the Console.");
			stopConsole();
			return;
		}
		
		currentMessageIndex++;
		if (currentMessageIndex >= messages.length) {
			currentMessageIndex = 0;
		}
		console.log("shifting to next Console message.");
		
		var enterDeferred = Console.enter(headerElem, messageElem, messages[currentMessageIndex]["header"],
										  messages[currentMessageIndex]["message"]);
		if (!enterDeferred || !enterDeferred.hasOwnProperty("done")) {
			console.error("Console.enter did not return a jQuery Deferred.");
			stopConsole();
			return;
		}
		
		enterDeferred.done(function () {
			setTimeout(function () {
				var exitDeferred = Console.exit(headerElem, messageElem);
				if (!exitDeferred || !exitDeferred.hasOwnProperty("done")) {
					console.error("Console.exit did not return a jQuery Deferred.");
					stopConsole();
					return;
				}
				
				exitDeferred.done(function () {
					runConsole();
				});
			}, Console.DISPLAY_TIME);
		});
	};
	
	Console.addMessage = function (header, message) {
		messages.push({'header': header, 'message': message});
	};
	
	Console.enter = function (headerElem, messageElem, header, message) {
		// virtual method
		// returns a jQuery Deferred
	};
	
	Console.exit = function (headerElem, messageElem) {
		// virtual method
		// returns a jQuery Deferred
	};
	
	Console.whitewash = function (headerElem, messageElem) {
		// virtual method
	};
	
	Console.start = function () {
		if (messages.length == 0) {
			console.error("Console.start() called when there are no messages in the queue.");
			return;
		}
		if (isConsoleActive) {
			return;
		}
		
		
		var headerElem = $(Console.HEADER_ELEM_SELECTOR),
			messageElem = $(Console.MESSAGE_ELEM_SELECTOR);
			
		if (!headerElem || !messageElem) {
			console.error("Could not locate one or more of the required DOM elements for the Console, in Console.start().");
			return;
		}
		
		isConsoleActive = true;
		Console.whitewash(headerElem, messageElem);
		console.log("Console has been started.");
		runConsole();
	};
})();