function nameChangeDialog() {
	var frame = $(document.createElement("div")),
		mainText = $(document.createElement("span"));
		
	mainText.html("Hey there! So, I noticed that you reached this page from <u>jacob.deitloff.com</u>. " +
		"I'm really excited that you " +
		"want to check out my page, so that's why I want to just quickly explain something!<br /><br />" +
		"I'm in the early pre-stages of a <strong>name change</strong>. Yeah! I'm changing my name from <strong>" +
		"Jacob</strong> to <strong>Alec</strong>!<br /><br />However, I haven't <i>legally</i> changed my name (yet). " +
		"So, when I'm using my legal information, I'll still list <u>jacob.deitloff.com</u> as my website " +
		"<strong>(to avoid confusion)</strong>. I've already redirected you, " +
		"but I wanted to make sure to explain it &mdash; that <strong>Alec = Jacob</strong>!").appendTo(frame);
	
	frame.appendTo(document.body).dialog({
		modal: true,
		resizable: false,
		draggable: false,
		title: "Let me clear up some confusion!",
		width: 400,
		buttons: [
			{
				text: "Ok",
				click: function () {
					$(this).dialog("close");
				}
			}
		]
	});
}