(function () {
	/** CONSOLE **/
	Console.enter = function (headerElem, messageElem, header, message) {
		var deferred = $.Deferred();
		
		headerElem.text(header).fadeIn(1000);
		messageElem.text(message).fadeIn(1001, function () {
			deferred.resolve();
		});
		
		return deferred.promise();
	};
	
	Console.exit = function (headerElem, messageElem) {
		var deferred = $.Deferred();
		
		headerElem.fadeOut(1000);
		messageElem.fadeOut(1001, function () {
			deferred.resolve();
		});
		
		return deferred.promise();
	};
})();