var headerElement = null;
var headerBackground = null;
var headerLogo = null;
var headerHead = null;
var headerOffset = null;
var headerMario = null;
document.onmousemove = function(e)
{
    if (!e)
    {
        var e = window.event;
    }
    
    if (headerBackground == null)
    {
        headerBackground = document.getElementById("headerBackground");
        if (headerBackground == null)
        {
            return;
        }
    }
    if (headerElement == null)
    {
        headerElement = document.getElementById("header");
        if (headerElement == null)
        {
            return;
        }
    }
    if (headerLogo == null)
    {
        headerLogo = document.getElementById("headerLogo");
        if (headerLogo == null)
        {
            return;
        }
    }
	if (headerHead == null)
	{
		headerHead = document.getElementById("headerHead");
		if (headerHead == null)
		{
			return;
		}
	}
    if (headerMario == null)
    {
        headerMario = document.getElementById("mario");
        if (headerMario == null)
        {
            return;
        }
    }
    
    headerOffset = (-128 * (e.pageX / self.innerWidth));
    headerBackground.className = "header frame" + (Math.floor(5 * (e.pageX / self.innerWidth)) + 1);
    headerElement.style.left = headerOffset + "px";
    headerBackground.style.backgroundPositionX = headerOffset + "px";
    headerLogo.style.right = (100 + headerOffset) + "px";
	headerHead.style.right = (-20 + headerOffset) + "px";
    headerMario.style.bottom = (35 + 32 * Math.sin((e.pageX / self.innerWidth) * Math.PI * 5 / 8)) + "px";//(48 * (e.pageX / self.innerWidth))) + "px";
    headerMario.style.right = (380 - (128 * (e.pageX / self.innerWidth))) + "px";
    //headerElement.style.backgroundPositionX = (-128 * (e.pageX / self.innerWidth)) + "px";
}