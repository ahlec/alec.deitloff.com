function questionVote(askId)
{
  var AJAX;
  try
  {
    AJAX = new XMLHttpRequest();
  }
  catch(e)
  {
    try
    {
      AJAX = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch(e)
    {
      try
      {
        AJAX = new ActiveXObject("Microsoft.XMLHTTP");
      }
      catch(e)
      {
        alert("Your browser does not support AJAX.");
        return false;
      }
    }
  }
  AJAX.onreadystatechange = function()
  {
    if (AJAX.readyState == 4)
    {
      if (AJAX.status == 200)
	  {
		if (AJAX.responseText.indexOf("success-") == 0)
		{
			alert("Your vote was counted! Thank you!");
			document.getElementById("votes-" + askId).innerHTML = AJAX.responseText.substring(8) + " <span class=\"label\">votes</span>";
		}
		else
		{
			alert("There was a problem, and we couldn't count your vote. But, that shouldn't stop you from spamming it anyways! :)");
		}
      }
	  else
        alert("There was a problem, and we couldn't count your vote. But, that shouldn't stop you from spamming it anyways! :)");
    }
  }
  AJAX.open("get", WEB_ROOT + "/scripts/questionVote.php?id=" + askId, true);
  AJAX.send(null);
}

function questionsSort(columnName)
{
  var AJAX;
  try
  {
    AJAX = new XMLHttpRequest();
  }
  catch(e)
  {
    try
    {
      AJAX = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch(e)
    {
      try
      {
        AJAX = new ActiveXObject("Microsoft.XMLHTTP");
      }
      catch(e)
      {
        alert("Your browser does not support AJAX.");
        return false;
      }
    }
  }
  AJAX.onreadystatechange = function()
  {
    if (AJAX.readyState == 4)
    {
      if (AJAX.status == 200)
	  {
		if (AJAX.responseText == "success")
		{
			document.location.reload(true);
		}
		else
		{
			alert("There was a problem and we couldn't change your sort preferences. Sorry :(");
		}
      }
	  else
        alert("There was a problem and we couldn't change your sort preferences. Sorry :(");
    }
  }
  AJAX.open("get", WEB_ROOT + "/scripts/questionsSort.php?column=" + columnName, true);
  AJAX.send(null);
}