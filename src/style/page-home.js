function keydownEventHandler(e, currentPageNo, hasPreviousPage, hasNextPage)
{
	if ($("div.photobooth[data-version]").length > 0) {
		return;
	}
	
    if (!e)
    {
        var e = window.event;
    }
    var keyCode = (e.keyCode || e.which);
    
    if (keyCode == 27)
    {
        // Escape
        if (document.getElementById("photobooth"))
        {
            closePhotobooth();
            return;
        }
    }
    else if (keyCode == 39)
    {
        // Right arrow key
        if (document.getElementById("photobooth"))
        {
        }
        else if (hasNextPage)
        {
            location.href = "/home/page-" + String(currentPageNo + 1) + "/";
            return;
        }
    }
    else if (keyCode == 37)
    {
        // Left arrow key
        if (document.getElementById("photobooth"))
        {
        }
        else if (hasPreviousPage)
        {
            if (currentPageNo <= 2)
            {
                location.href = "/";
            }
            else
            {
                location.href = "/home/page-" + String(currentPageNo - 1) + "/";
            }
            return;
        }
    }
}

$.photobooth = function(event) {
	var currentImg = $(this);
	var data = currentImg.data("photobooth");
	var previousImg = $(this).prev("img[data-photobooth]");
	var previous = previousImg.data("photobooth");
	if (previous != undefined) {
		if (typeof previous === 'string') {
			previous = previousImg.attr('src');
		}
		else {
			previous = previous.url;
		}
	}
	var nextImg = $(this).next("img[data-photobooth]");
	var next = nextImg.data("photobooth");
	if (next != undefined) {
		if (typeof next === 'string') {
			next = nextImg.attr('src');
		}
		else {
			next = next.url;
		}
	}
	
	// create the photobooth
	var originalYOffset = (window.pageYOffset || document.documentElement.scrollTop);
	var curtains = $(document.createElement("div"));
	curtains.addClass("photobooth").data("version", "2.0").attr("id", "photobooth").click(function () {
		scroll(0, originalYOffset);
		$("body").removeClass("photoboothActive").unbind("keydown.photoboothv2");
		curtains.remove();
	});
	
	var curtainsBox = $(document.createElement("div"));
	curtains.append(curtainsBox);
	
	var curtainsBoxCell = $(document.createElement("div"));
	curtainsBox.append(curtainsBoxCell);
	
	var photo = $(document.createElement("img"));
	photo.css("max-width",
			Math.round(screen.availWidth * 0.8) + "px").css("max-height",
			Math.round(screen.availHeight * 0.8) + "px");
	if (typeof data === 'string') {
		photo.attr("src", WEB_ROOT + currentImg.attr("src"));
	}
	else {
		photo.attr("src", WEB_ROOT + data.url);
	}
	curtainsBoxCell.append(photo);
	
	$("body").first().append(curtains).addClass("photoboothActive").bind("keydown.photoboothv2", function (e) {
		if (e.which == 27) {
			// escape
			e.stopImmediatePropagation();
			curtains.click();
		} else if (e.which == 39 && next != undefined) {
			// right arrow key
			photo.attr("src", WEB_ROOT + next);
			previousImg = currentImg;
			currentImg = nextImg;
			nextImg = nextImg.next("img[data-photobooth]");
			previous = data;
			data = next;
			if (nextImg != undefined) {
				next = nextImg.data("photobooth");
				if (typeof next === 'string') {
					next = nextImg.attr('src');
				}
				else {
					next = next.url;
				}
			}
			else {
				next = undefined;
			}
			e.stopImmediatePropagation();
		} else if (e.which == 37 && previous != undefined) {
			// left arrow key
			photo.attr("src", WEB_ROOT + previous);
			nextImg = currentImg;
			currentImg = previousImg;
			previousImg = previousImg.prev("img[data-photobooth]");
			next = data;
			data = previous;
			if (previousImg != undefined) {
				previous = previousImg.data("photobooth");
				if (typeof previous === 'string') {
					previous = previousImg.attr('src');
				}
				else {
					previous = previous.url;
				}
			}
			else {
				previous = undefined;
			}
			e.stopImmediatePropagation();
		}
	});
	scroll(0, 0);
};

$(document).ready(function () {
	$("img[data-photobooth]").click($.photobooth);
});

function photobooth(imagePath)
{
    if (document.getElementById("photobooth"))
    {
        return;
    }
    
    var curtains = document.createElement("div");
    curtains.className = "photobooth";
    curtains.setAttribute("id", "photobooth");
    
    var curtainsBox = document.createElement("div");
    curtains.appendChild(curtainsBox);
    var curtainsBoxCell = document.createElement("div");
    curtainsBox.appendChild(curtainsBoxCell);
    
    var photo = document.createElement("img");
    photo.src = WEB_ROOT + imagePath;
    photo.style.maxHeight = Math.round(screen.availHeight * 0.8) + "px";
    photo.style.maxWidth = Math.round(screen.availWidth * 0.8) + "px";
    curtainsBoxCell.appendChild(photo);
    
    curtains.onclick = closePhotobooth;
    
    curtains.originalYOffset = (window.pageYOffset || document.documentElement.scrollTop);
    document.body.appendChild(curtains);
    document.body.className += " photoboothActive";
    scroll(0, 0);
}

function closePhotobooth()
{
    var photobooth = document.getElementById("photobooth");
    scroll(0, photobooth.originalYOffset);
    document.body.className = document.body.className.replace(" photoboothActive", "");
    document.body.removeChild(photobooth);
}