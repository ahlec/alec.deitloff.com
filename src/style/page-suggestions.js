function suggestions_vote(suggestion_id)
{
  var AJAX;
  try
  {
    AJAX = new XMLHttpRequest();
  }
  catch(e)
  {
    try
    {
      AJAX = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch(e)
    {
      try
      {
        AJAX = new ActiveXObject("Microsoft.XMLHTTP");
      }
      catch(e)
      {
        alert("Your browser does not support AJAX.");
        return false;
      }
    }
  }
  AJAX.onreadystatechange = function()
  {
    if (AJAX.readyState == 4)
    {
      if (AJAX.status == 200)
	  {
		if (AJAX.responseText.indexOf("success-") == 0)
		{
			alert("Your vote was counted! Thank you!");
			document.getElementById("votes-" + suggestion_id).innerHTML = AJAX.responseText.substring(8);
		}
		else
		{
			alert("There was a problem, and we couldn't count your vote. But, that shouldn't stop you from spamming it anyways! :)");
		}
      }
	  else
        alert("There was a problem, and we couldn't count your vote. But, that shouldn't stop you from spamming it anyways! :)");
    }
  }
  AJAX.open("get", WEB_ROOT + "/scripts/suggestions_vote.php?id=" + suggestion_id, true);
  AJAX.send(null);
}

function suggestions_sort(column_name)
{
  var AJAX;
  try
  {
    AJAX = new XMLHttpRequest();
  }
  catch(e)
  {
    try
    {
      AJAX = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch(e)
    {
      try
      {
        AJAX = new ActiveXObject("Microsoft.XMLHTTP");
      }
      catch(e)
      {
        alert("Your browser does not support AJAX.");
        return false;
      }
    }
  }
  AJAX.onreadystatechange = function()
  {
    if (AJAX.readyState == 4)
    {
      if (AJAX.status == 200)
	  {
		if (AJAX.responseText == "success")
		{
			document.location.reload(true);
		}
		else
		{
			alert("There was a problem and we couldn't change your sort preferences. Sorry :(");
		}
      }
	  else
        alert("There was a problem and we couldn't change your sort preferences. Sorry :(");
    }
  }
  AJAX.open("get", WEB_ROOT + "/scripts/suggestions_sort.php?column=" + column_name, true);
  AJAX.send(null);
}